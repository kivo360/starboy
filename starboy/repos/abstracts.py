from __future__ import annotations

# Standard Library
import abc
from contextlib import suppress
from datetime import datetime
from operator import attrgetter as attrgt
import sys
import time
from typing import Any, cast, Dict, List, Optional, Type, Union

from github import Github
from github import InputGitTreeElement
# from github import Organization
from github.Organization import Organization
from github.Repository import Repository
from inflection import underscore

# Development Tools
from addict import Addict
from devtools import debug
import devtools as deev
from loguru import logger
from pathlib import Path
from pathlib import PurePath

# Pydantic and Serialization
from pydantic import BaseModel
from pydantic import BaseSettings
from pydantic import DirectoryPath
from pydantic import Field
from pydantic import FilePath
from pydantic.class_validators import root_validator
from pydantic.class_validators import validator
from pydantic.types import NoneStr

# Starboy
from starboy.steps import get_faas_root

CompareDict = Dict[str, 'Comparators']

# from operator import
MONOREPO_KEYS = {"serverless", "services", "covered"}


class GithubSettings(BaseSettings):
    # auth_key: str
    personal_token: str = Field(env='GITHUB_PERSONAL_ACCESS')
    faas_prefix: str = "faas_memez_"
    org_name: str = "bodhi-sync"


def get_or_create_main(fn_name: str, org: Organization):
    # Create a function that returns the create file.
    fn_repo = org.get_repo(fn_name)
    capn = fn_name.capitalize()
    main_branch = None
    with suppress(Exception):
        main_branch = fn_repo.get_branch("main")
        return main_branch, fn_repo
    fn_repo.create_file("README.md", f"First commit for {fn_name}",
                        f"# Serverless Function {capn} \nFile Content")
    main_branch = fn_repo.get_branch("main")
    return main_branch, fn_repo


def create_new_commit(fn_layer: str, input_el: List[InputGitTreeElement],
                      org: Organization):
    _mbranch, _repo = get_or_create_main(fn_layer, org=org)
    _head_ref = list(_repo.get_git_refs())[0]
    latest_commit = _repo.get_git_commit(_head_ref.object.sha)
    base_tree = _repo.get_git_tree(_head_ref.object.sha)
    new_tree = _repo.create_git_tree(input_el, base_tree)
    new_commit = _repo.create_git_commit(
        message=f"Updating the faas function {fn_layer} ... {datetime.now()}",
        parents=[latest_commit],
        tree=new_tree)
    _head_ref.edit(sha=new_commit.sha, force=False)
    return _head_ref, new_commit, _repo, _mbranch


# def get_faas_repos(repo_list: List[Repository]) -> List[MonoItem]:

#     faas_repos: List[MonoItem] = []
#     for repo in repo_list:
#         fn_name = repo.name
#         if is_faas(fn_name):
#             faas_repos.append(MonoItem(name=fn_name, path=repo.full_name))
#     return faas_repos


class UnreasonFlex(BaseModel):
    """
    UnreasonFlex

    Unreasonable Flexibility

    Just a BaseModel that has the config setup for easily entering in information of different kinds.

    """
    class Config:
        arbitrary_types_allowed = True
        extras = 'allowed'


class ServerlessFn(UnreasonFlex):
    config_file: FilePath
    fn_dir: DirectoryPath
    input_elements: List[InputGitTreeElement] = []

    @root_validator
    def gather_files(cls, values: Dict[str, Any]):
        loc_dict = Addict(**values)
        loc_dict.input_elements = []
        _details = loc_dict.fn_dir
        for cont in _details.rglob("*"):
            is_file = cont.is_file()
            if is_file:
                rel_path = cont.relative_to((get_faas_root()))
                rel_path_str = str(rel_path)
                print(rel_path_str)
                file_content = cont.open().read()
                in_element = InputGitTreeElement(path=rel_path_str,
                                                 mode='100755',
                                                 type='blob',
                                                 content=file_content)
                loc_dict.input_elements.append(in_element)
        file_content = loc_dict.config_file.open().read()
        in_element = InputGitTreeElement(path=loc_dict.config_file.name,
                                         mode='100755',
                                         type='blob',
                                         content=file_content)
        loc_dict.input_elements.append(in_element)

        return loc_dict.to_dict()


class MonoItem(UnreasonFlex):
    name: str
    path: Optional[Union[Path, str]]


class MonoState(UnreasonFlex, abc.ABC):
    type_name: str
    pre: bool = False
    remote: bool = False
    serverless: List[MonoItem] = []
    services: List[MonoItem] = []
    covered: List[MonoItem] = []

    @property
    def key_states(self) -> Dict[str, Any]:
        return self.dict(include=MONOREPO_KEYS)

    @property
    def key_cpy(self) -> 'MonoState':
        return self.copy(include=MONOREPO_KEYS)


class Rule(UnreasonFlex):
    opt_name: str

    @abc.abstractmethod
    def run(self, rinput) -> None:
        pass


class Comparators(MonoState):
    remote: bool = True
    type_name: str = "comparers"
    operation_name: Optional[str] = None

    parent: Optional['StateController'] = None

    @validator('operation_name', pre=True, always=True)
    def set_opt_name(cls, v: NoneStr) -> str:
        return v or underscore(cls.__name__)  # type: ignore

    @abc.abstractmethod
    def compare(self) -> 'Comparators':
        raise NotImplementedError


RuleInsts = List[Rule]
RuleTypes = List[Type[Rule]]


class RuleProcessor(UnreasonFlex):
    """
    StateProcessor

        Takes the current state and determines what to do next. Goes along side the comparison class.

        Resembles pants builds runner class. Rules will be coded in directly for time's sake.

    """
    # name: Optional[str] = None  # gets name of specific processors
    # placeholder_rules: List[Type[Rule]] = [CreateRepos]
    rules: RuleTypes = []
    rule_dict: Dict[str, Rule] = {}
    parent: StateController

    def get_rule(self, name: str) -> Optional[Rule]:
        return self.rule_dict.get(name, None)

    @abc.abstractmethod
    def run(self, *args, **kwargs) -> None:
        """
        run Runs a rule set to be used with the controller.

        Will implement it's own rule running logic depending on what subclasses it.
        The process will vary, though the outcome will be the same.

        """


class StateController(UnreasonFlex):
    origin: Union[MonoState, Dict[str, Dict[str, List[Dict[str, str]]]],
                  Dict[str, Any]]
    target: MonoState
    comparisons: List['Comparators'] = []
    processors: Optional['RuleProcessor'] = None

    unsynced: CompareDict = {}

    def __init__(self, **data: Any) -> None:  # type: ignore
        super().__init__(**data)
        self.comparisons = self.add_parent()
        self.run_comparison()

    def add_parent(self) -> List[Comparators]:
        comp_val = []
        for com in self.comparisons:
            com.parent = self
            comp_val.append(com)
        return comp_val

    def unsync_by(self, opt: str) -> Optional[Comparators]:
        return self.unsynced.get(opt, None)

    def run_comparison(self) -> CompareDict:
        # logger.debug("Starting mass gap comparisons ...")
        for comp in self.comparisons:
            comp_out = comp.compare()
            opt = comp_out.operation_name
            if opt is None:
                continue
            _core = comp_out.key_cpy
            self.unsynced[opt] = _core  # type: ignore
        return self.unsynced

    def is_unsync(self, name: str) -> bool:
        return name in self.unsynced


Comparators.update_forward_refs()


class DevState(MonoState):
    type_name: str = "local"
    pre: bool = True


class RemoteState(MonoState):
    remote: bool = True
    caller: Any = None

    def __init__(self, **data: Any) -> None:  # type: ignore
        super().__init__(**data)
        self.load_settings()

    @abc.abstractmethod
    def load_settings(self) -> None:
        raise NotImplementedError
