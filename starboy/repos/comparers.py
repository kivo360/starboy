# Standard Library
from operator import attrgetter
from typing import Any, cast, List, Optional

from toolz.curried import get
from toolz.curried import groupby
from toolz.curried import pluck
from toolz.curried import valmap

# Development Tools
from devtools import debug
import devtools as deev
from loguru import logger
from pathlib import Path
from pathlib import PurePath

# Pydantic and Serialization
from pydantic import validator
from pydantic.types import NoneStr

# Starboy
from starboy.repos.abstracts import Comparators
from starboy.repos.abstracts import MonoItem
from starboy.repos.abstracts import MONOREPO_KEYS
from starboy.repos.abstracts import MonoState
from starboy.repos.cmds import name_in


class RepoNotExist(Comparators):
    def compare(self) -> 'Comparators':
        org: MonoState = self.parent.origin  # type: ignore
        trg: MonoState = self.parent.target  # type: ignore
        tckey = trg.key_states
        for cat_name, val in org.key_cpy:
            item_list: List[Any] = cast(List[Any], tckey.get(cat_name))
            for itm in val:
                iname: str = itm.name
                if not name_in(item_list, itm.name):
                    assets: List[MonoItem] = attrgetter(cat_name)(self)
                    assets.append(itm)
                    setattr(self, cat_name, assets)
        MAIN_KEYS = MONOREPO_KEYS | {"operation_name", "type_name"}
        outcome = self.copy(include=MAIN_KEYS)
        return outcome


# RepoNotExist.update_forward_refs()


class RepoDataChange(Comparators):
    operation_name: Optional[str] = "repo_commit_files"

    def compare(self) -> 'Comparators':
        org: MonoState = self.parent.origin  # type: ignore
        trg: MonoState = self.parent.target  # type: ignore
        tckey = trg.key_states
        # logger.error(tckey)
        for cat_name, val in org.key_cpy:
            setattr(self, cat_name, val)
        MAIN_KEYS = MONOREPO_KEYS | {"operation_name", "type_name"}
        outcome = self.copy(include=MAIN_KEYS)
        # deev.debug(outcome)
        return outcome


# RepoDataChange.update_forward_refs()
