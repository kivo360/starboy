from __future__ import annotations

# Standard Library
import abc
from contextlib import suppress
from os import EX_CANTCREAT
from os import popen
import random
import sys
import time
from typing import Any, cast, Dict, List, Optional, Union

from faker import Faker
from github import Github
# from github import Organization
from github.Organization import Organization
from github.Repository import Repository
from toolz import groupby

# Development Tools
from addict import Addict
import devtools as deev
from loguru import logger
from pathlib import Path

# Pydantic and Serialization
from pydantic import Field
from pydantic import root_validator
from pydantic import validator
from pydantic.main import BaseModel

# Starboy
from starboy.repos.abstracts import Comparators
from starboy.repos.abstracts import DevState
from starboy.repos.abstracts import GithubSettings
from starboy.repos.abstracts import MonoItem
from starboy.repos.abstracts import RemoteState
from starboy.repos.abstracts import Rule
from starboy.repos.abstracts import RuleProcessor
from starboy.repos.abstracts import RuleTypes
from starboy.repos.abstracts import ServerlessFn
from starboy.repos.abstracts import StateController
from starboy.repos.cmds import create_new_commit
from starboy.repos.cmds import get_faas_repos
from starboy.repos.cmds import name_in
from starboy.repos.cmds import to_git_faas
from starboy.repos.comparers import RepoDataChange
from starboy.repos.comparers import RepoNotExist

# from inflection

CompareDict = Dict[str, 'Comparators']

# logger.add(sys.stdout, backtrace=False)
# logger.add(sys.stderr, backtrace=False)

# from operator import
MONOREPO_KEYS = {"serverless", "services", "covered"}
LOG_TYPESET = [logger.success, logger.error, logger.warning, logger.debug]


class GitRules(Rule):
    opt_name: str
    api: Optional[Github] = None
    settings: GithubSettings = GithubSettings()

    @property
    def org(self) -> Organization:
        """
        org Get organization

        Returns:
            Organization: A organization github object.
        """
        if self.api is None:
            raise AttributeError("The github api was not found.")
        return self.api.get_organization(self.settings.org_name)

    @property
    def org_repos(self) -> List[Repository]:
        return list(self.org.get_repos())

    def load_settings(self):
        self.serverless = get_faas_repos(self.org_repos)

    @abc.abstractmethod
    def run(self, rinput: Any) -> None:
        pass


# ---------------------------------------------------------------------------------------
# ------------------------- My direct github command goes here ---------------------------
# ---------------------------------------------------------------------------------------


def create_github_repo(org_api: Organization, repo_name: str):
    logger.info(f"Attempting to create repository  for {repo_name}")
    try:
        org_api.create_repo(
            to_git_faas(repo_name),
            auto_init=True,
            description=
            "Tempor qui velit enim cupidatat excepteur ullamco do cupidatat commodo velit duis."
        )
    except Exception as e:
        logger.debug(e)


def remove_github_repo(org_api: Organization, repo_name: str):
    logger.error(f"Attempting to delete repository for {repo_name}")
    try:
        org_api.get_repo(to_git_faas(repo_name)).delete()
    except Exception as e:
        logger.error(e)


def update_log():
    return random.choice(
        [logger.success, logger.error, logger.warning, logger.debug])


class CreateRepos(GitRules):
    opt_name: str = 'repo_not_exist'

    def is_runnable(self) -> bool:
        if self.api is None:
            return False
        elif self.settings is None:
            return False
        return True

    def run(self, rinput: Union[Comparators]):
        if not self.is_runnable():
            return
        removal_list = []
        for _, item_details in rinput.dict().items():
            logger.info(_)
            # logger.info(item_details)
            if not isinstance(item_details, list):
                continue
            if len(item_details) <= 0:
                continue
            # item: MonoItem
            # logger.warning(item_details)
            for item in item_details:
                # logger.debug(item)

                # is_any = any(isinstance(x, MonoItem) for x in item_details)
                # if not is_any:
                #     continue
                item = MonoItem(**item)
                logger.info(item)
                removal_list.append(item.name)
                create_github_repo(self.org, item.name)

        time.sleep(10)
        # for rm_name in removal_list:
        #     remove_github_repo(self.org, rm_name)


class OutputConfig(BaseModel):
    config: Any
    content: Any


class RepoCommitFiles(GitRules):
    opt_name: str = 'repo_commit_files'

    def is_runnable(self) -> bool:
        if self.api is None:
            return False
        elif self.settings is None:
            return False
        return True

    def standardize_config(self, path_set: List[MonoItem] = []):
        if not path_set:
            raise AttributeError("There are no elements inside of the group.")
        elif len(path_set) != 2:
            raise ValueError("Need to have exactly two items.")

        # WARNING: BAD CODE HERE
        _config = None
        _content = None
        item: MonoItem
        for item in path_set:
            loc_path = cast(Path, item.path)
            if loc_path.is_file():
                _config = loc_path
                continue
            _content = loc_path
        if not _config or not _content:
            raise AttributeError("One of both attributes is not there.")
        return OutputConfig(config=_config, content=_content)

    def run(self, rinput: Union[Comparators]):
        group_srv = groupby(lambda x: x.name, rinput.serverless)
        for name, path_set in group_srv.items():
            with logger.catch():
                resp: OutputConfig = self.standardize_config(path_set=path_set)
                git_faas = ServerlessFn(config_file=resp.config,
                                        fn_dir=resp.content)

                create_new_commit(to_git_faas(name),
                                  input_el=git_faas.input_elements,
                                  org=self.org)
                random.choice(LOG_TYPESET)(name)
                random.choice(LOG_TYPESET)(git_faas)
                # poop_mono: MonoItem = path_set[0]
                # poop_file: Path = cast(Path, poop_mono.path)
                # file_msg: str = "Is a file" if poop_file.is_file(
                # ) else "Is either missing or a dir"
                # miss_msg: str = "Is not missing" if poop_file.exists(
                # ) else "Does not exist."
                # logger.debug(name)
                # logger.info(poop_file)
                # logger.warning(file_msg)
                # logger.success(miss_msg)


class GithubState(RemoteState):
    type_name: str = "github"
    settings: GithubSettings = GithubSettings()
    api: Github = None  # type: ignore

    @validator('api', pre=True, always=True)
    def valid_api(cls, v: Optional[Github], values: Dict[str, Any],
                  **kwargs) -> Github:
        settings: GithubSettings = values.get("settings")  # type: ignore
        return v or Github(settings.personal_token)

    @property
    def org(self) -> Organization:
        """
        org Get organization

        Returns:
            Organization: A organization github object.
        """
        return self.api.get_organization(self.settings.org_name)

    @property
    def org_repos(self) -> List[Repository]:
        return list(self.org.get_repos())

    def load_settings(self):
        self.serverless = get_faas_repos(self.org_repos)


class GithubProcessor(RuleProcessor):

    rules: RuleTypes = [CreateRepos, RepoCommitFiles]
    parent: StateController

    def __init__(__pydantic_self__, **data: Any) -> None:
        super().__init__(**data)

    # @logger.catch
    @root_validator
    def git_root(cls, values):
        adt_vals = Addict(**values)
        github_rules = adt_vals.rules
        git_dict = adt_vals.rule_dict
        parent: GithubState = adt_vals.parent
        local_api: Github = parent.api
        local_settings: GithubSettings = parent.settings
        rulez: RuleTypes = []
        for rule in github_rules:
            new_rule = rule(api=local_api, settings=local_settings)
            git_dict[new_rule.opt_name] = new_rule
            rulez.append(new_rule)
        adt_vals.rule_dict = git_dict
        adt_vals.rules = rulez
        return adt_vals.to_dict()

    def run(self, *args, **kwargs) -> None:
        compared = Addict(self.parent.unsynced)

        if not compared:
            return

        for key, rule in self.rule_dict.items():
            logger.info(f"Running the rule: {key}")
            if key not in compared:
                continue

            rule.run(compared[key])

        deev.debug(list(compared.keys()))


class GithubController(StateController):
    target: GithubState
    comparisons: List['Comparators'] = [RepoNotExist(), RepoDataChange()]
    processors: Optional['RuleProcessor'] = None
    settings: GithubSettings = GithubSettings()
    api: Github = None  # type: ignore

    def __init__(self, **data: Any) -> None:
        super().__init__(**data)
        self.valid_processors()
        if self.processors is not None:
            self.processors.run(compared=self)

    @validator('api', pre=True, always=True)
    def valid_api(cls, v: Optional[Github], values: Dict[str, Any],
                  **kwargs) -> Github:
        settings: GithubSettings = values.get("settings")  # type: ignore
        return v or Github(settings.personal_token)

    def valid_processors(self, *args, **kwargs) -> None:
        self.processors = GithubProcessor(api=self.api, parent=self)

    @property
    def org(self) -> Organization:
        """
        org Get organization

        Returns:
            Organization: A organization github object.
        """
        return self.api.get_organization(self.settings.org_name)

    @property
    def org_repos(self) -> List[Repository]:
        return list(self.org.get_repos())

    def load_settings(self):
        self.serverless = get_faas_repos(self.org_repos)


GithubController.update_forward_refs()


def main() -> None:
    local_state = {
        "serverless": [{
            "name": "chatbot",
            "path": "/tmp/turtule"
        }, {
            "name": "scanner",
            "path": "/tmp/scanner"
        }, {
            "name": "chico",
            "path": "/tmp/covered"
        }, {
            "name": "chico",
            "path": "/tmp/covered.yml"
        }],
        "services": [],
        "covered": [],
    }
    local_state = DevState(**local_state)  # type: ignore
    git_state = GithubState()
    # logger.info(DevState(**local_state))
    git_control = GithubController(origin=local_state, target=git_state)
    # git_control.add_parent()
    # # deev.debug(git_control.compars[0])


if __name__ == "__main__":
    # TODO: Refactor to have all github accessible objects either inherit the same class or take the StateController as the parent.
    # The state controller wouuld be most consistent between all classes.
    main()
