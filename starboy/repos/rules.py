from __future__ import annotations

# Standard Library
import abc
import sys
from typing import Any, Dict, List, Optional

from github import Github
# from github import Organization
from github.Organization import Organization
from github.Repository import Repository

# Development Tools
from addict import Addict
from loguru import logger

# Pydantic and Serialization
from pydantic import BaseSettings
from pydantic import Field
from pydantic import root_validator
from pydantic import validator

# Starboy
from starboy.repos.abstracts import Comparators
from starboy.repos.abstracts import GithubSettings
from starboy.repos.abstracts import Rule
# from starboy.steps import get_faas_root
from starboy.repos.cmds import get_faas_repos

CompareDict = Dict[str, 'Comparators']

# from operator import
MONOREPO_KEYS = {"serverless", "services", "covered"}


class GitRules(Rule):
    opt_name: str
    api: Optional[Github] = None
    settings: GithubSettings = GithubSettings()

    @property
    def org(self) -> Organization:
        """
        org Get organization

        Returns:
            Organization: A organization github object.
        """
        if self.api is None:
            raise AttributeError("The github api was not found.")
        return self.api.get_organization(self.settings.org_name)

    @property
    def org_repos(self) -> List[Repository]:
        return list(self.org.get_repos())

    def load_settings(self):
        self.serverless = get_faas_repos(self.org_repos)

    @abc.abstractmethod
    def run(self, rinput: Any) -> None:
        pass
