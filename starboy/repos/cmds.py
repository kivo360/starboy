from __future__ import annotations

# Standard Library
from contextlib import suppress
from datetime import datetime
import sys
from typing import List

from github import InputGitTreeElement
# from github import Organization
from github.Organization import Organization
from github.Repository import Repository

# Development Tools
from loguru import logger

# Pydantic and Serialization
from pydantic import validate_arguments

# Starboy
from starboy.repos.abstracts import GithubSettings
from starboy.repos.abstracts import MonoItem

# from operator import
MONOREPO_KEYS = {"serverless", "services", "covered"}

git_set = GithubSettings()


def get_or_create_main(fn_name: str, org: Organization):
    # Create a function that returns the create file.
    fn_repo = org.get_repo(fn_name)
    capn = fn_name.capitalize()
    main_branch = None
    with suppress(Exception):
        main_branch = fn_repo.get_branch("main")
        return main_branch, fn_repo
    fn_repo.create_file("README.md", f"First commit for {fn_name}",
                        f"# Serverless Function {capn} \nFile Content")
    main_branch = fn_repo.get_branch("main")
    return main_branch, fn_repo


def create_new_commit(fn_layer: str, input_el: List[InputGitTreeElement],
                      org: Organization):
    main_branch, proj_repo = get_or_create_main(fn_layer, org=org)
    head_reference = list(proj_repo.get_git_refs())[0]
    latest_commit = proj_repo.get_git_commit(head_reference.object.sha)
    base_tree = proj_repo.get_git_tree(head_reference.object.sha)
    new_tree = proj_repo.create_git_tree(input_el, base_tree)
    new_commit = proj_repo.create_git_commit(
        message=f"Updating the faas function {fn_layer} ... {datetime.now()}",
        parents=[latest_commit],
        tree=new_tree)
    head_reference.edit(sha=new_commit.sha, force=False)
    return head_reference, new_commit, proj_repo, main_branch


def is_faas(repo_name: str) -> bool:
    return repo_name.startswith(git_set.faas_prefix)


def to_git_faas(fn_name: str) -> str:
    faas_name: str = f"{git_set.faas_prefix}{fn_name}"
    return faas_name


def get_faas_repos(repo_list: List[Repository]) -> List[MonoItem]:

    faas_repos: List[MonoItem] = []
    for repo in repo_list:
        fn_name = repo.name
        if is_faas(fn_name):
            faas_repos.append(MonoItem(name=fn_name, path=repo.full_name))
    return faas_repos


@validate_arguments
def name_in(item_list: List[MonoItem], name: str) -> bool:
    for item in item_list:
        if item.name == name:
            return True
    return False
