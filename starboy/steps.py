# Standard Library
from typing import Any, Dict, List, Optional

# Development Tools
from loguru import logger
from pathlib import Path

# Pydantic and Serialization
from pydantic import BaseModel
from pydantic import validate_arguments

# Starboy
from starboy import utils
from starboy.continuous.storage import init_store
from starboy.continuous.storage import StateDiffManager
from starboy.models import PathTraversal
from starboy.path_editor import FaasManager
from starboy.snapshots.core import SnapshotWrapper


def init_state_manager(path: Path):
    state_manager = init_store(path)
    state_manager.reset()
    return state_manager


def get_faas_root():
    traverser = PathTraversal()
    faas_root: Optional[Path] = traverser.get_root_by_name("faas")
    return faas_root


def get_faas_top(fss_root: Path) -> Dict[str, Any]:
    faas_man = FaasManager(fss_root)
    return faas_man.observed


def get_faas_snapshot(fss_root: Path) -> SnapshotWrapper:
    sn_wrp = SnapshotWrapper(fss_root)
    return sn_wrp


def filter_snap(top_fn: SnapshotWrapper, snap_fn: SnapshotWrapper):
    logger.warning("filtering files for compatibility")


def get_diffs(_state: StateDiffManager):
    return {
        "faas_top": _state.faas_diff,
        "snap_diff": _state.snap_diff,
    }


class PreLocalState(BaseModel):
    serverless: List[Dict[str, str]] = []
    covered: List[Dict[str, str]] = []
    services: List[Dict[str, str]] = []


@logger.catch
@validate_arguments
def get_faas_resources(mount_path: Path) -> Dict[str, List[Dict[str, str]]]:
    state = init_store(mount_path)
    fr = get_faas_root()
    top_fr = get_faas_top(fr)
    snap = get_faas_snapshot(fr)
    state.step(faas_in=top_fr, snapshot_in=snap)
    diffs = get_diffs(state)
    # TODO: Figure out why the top level difference management isn't working.
    filtered = utils.filter_faas_candidates(
        fr,
        diffs.get("snap_diff"),
        top_fr,
    )
    # logger.success("Going to upload the following files.")
    return PreLocalState(serverless=[{
        "name": x,
        "path": str(y)
    } for x, y in filtered]).dict()
