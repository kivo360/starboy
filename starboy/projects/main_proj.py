import typer, time
from pathlib import Path
from typing import Optional
from starboy.path_editor import RustProjectManager
from starboy.models import PantsModel, PathTraversal

# rust_pm =
traveller = PathTraversal()

app = typer.Typer()
add_app = typer.Typer(help="Add a new app")
remove_app = typer.Typer(help="Remove an app")

app_mgnt = typer.Typer()
app_mgnt.add_typer(add_app,
                   name="new",
                   help="Create a new application (using cargo and poetry).")
app_mgnt.add_typer(remove_app, name="remove", help="Remove an app")

dep_app = typer.Typer()

# Use to add or remove dependencies
dep_add = typer.Typer()
dep_rem = typer.Typer()

dep_app.add_typer(dep_add,
                  name='add',
                  help='Add depenency to a given project type.')
dep_app.add_typer(dep_rem,
                  name='rem',
                  help="Rem depenedency from a given project type.")

app.add_typer(app_mgnt,
              name="manage",
              help="Add or remove projects to the repo.")
app.add_typer(dep_app,
              name="dep",
              help="Manage dependencies (using poetry and cargo).")


@add_app.command('rust')
def new_rust_project(name: str,
                     library: bool = typer.Option(
                         False, help="Determines if this a library or not.")):
    """A single command that aims to install everything on the computer."""
    typer.secho("Adding new rust project", fg=typer.colors.BRIGHT_YELLOW)
    rust_root: Optional[Path] = traveller.get_root_by_name("rust")
    if not rust_root:
        raise AssertionError("The rust root doesn't exist")
    rust_pm: RustProjectManager = RustProjectManager(rust_root)
    rust_pm.add_project(name=name, library=library)
    npath = rust_root / name
    time.sleep(0.4)
    if not npath.exists():
        typer.secho("We weren't able to add the project",
                    fg=typer.colors.BRIGHT_RED)
        return

    typer.secho("Project successfully added.", fg=typer.colors.BRIGHT_GREEN)
    rust_pm.sync_subspace()


@remove_app.command('rust')
def rem_rust_project(name: str):
    """A single command that aims to install everything on the computer."""
    typer.secho("Adding new rust project", fg=typer.colors.BRIGHT_YELLOW)

    rust_root: Optional[Path] = traveller.get_root_by_name("rust")
    npath = rust_root / name
    if not npath.exists():
        typer.secho(f"The project '{name}' doesn't exist",
                    fg=typer.colors.BRIGHT_CYAN)
        return
    rust_pm: RustProjectManager = RustProjectManager(rust_root)
    rust_pm.remove_project(name=name)
    if npath.exists():
        typer.secho("Fuuuuuuuuuuuuuuuck", fg=typer.colors.BRIGHT_RED)
        return

    typer.secho("Project successfully removed.", fg=typer.colors.BRIGHT_GREEN)
    rust_pm.sync_subspace()
    rust_pm.cargo_purge(name=name)


@add_app.command('python')
def new_python_project():
    """Installs a redis instance to the computer using docker."""
    typer.secho("Adding a new python project", fg=typer.colors.CYAN)


@dep_add.command(name='rust')
def add_rust_dep(name: str,
                 project: str,
                 local: bool = typer.Option(
                     False, help="Determining if this is a local project")):
    """Add a rust depenedency"""
    typer.secho(f"Adding a new rust depenendency to {project}",
                fg=typer.colors.YELLOW)
    rust_root: Optional[Path] = traveller.get_root_by_name("rust")
    rust_pm: RustProjectManager = RustProjectManager(rust_root)
    rust_pm.cargo_install("cargo-edit")
    rust_pm.cargo_add(name, project, is_local=local)


@dep_add.command(name='python')
def add_python_dep(name: str, project: str):
    """Add a Python depenedency"""
    typer.secho(f"Adding a new python depenendency to {project}",
                fg=typer.colors.BRIGHT_MAGENTA)


@dep_rem.command(name='rust')
def rem_rust_dep(name: str, project: str):
    """Remove a Rust depenedency"""
    typer.secho(f"Adding a new python depenendency to {project}",
                fg=typer.colors.BRIGHT_MAGENTA)
    rust_root: Optional[Path] = traveller.get_root_by_name("rust")
    rust_pm: RustProjectManager = RustProjectManager(rust_root)
    rust_pm.cargo_install("cargo-edit")
    rust_pm.cargo_remove(name, project)


@dep_rem.command(name='python')
def rem_python_dep(name: str, project: str):
    """Remove a Python depenedency"""
    typer.secho(f"Adding a new python depenendency to {project}",
                fg=typer.colors.BRIGHT_MAGENTA)
