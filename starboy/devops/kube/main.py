import typer

kube_app = typer.Typer()

kube_app_create = typer.Typer()
kube_dev = typer.Typer()
kube_app.add_typer(
    kube_app,
    name="create",
    help="Multiple create commands for kubernetes work (service account, etc)")
kube_app.add_typer(
    kube_dev,
    name="dev",
    help=
    "A set of dev commands. Will eventually just store credentials in the typer dir."
)
