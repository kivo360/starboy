# Standard Library
import base64
import os
import time
from typing import List, Literal, Optional, Union

from inflection import camelize
from inflection import parameterize
from inflection import tableize
from inflection import underscore
from kubeconfig import KubeConfig
import typer

# Development Tools
from addict import Addict
from devtools import debug
from loguru import logger
import yaml

# Pydantic and Serialization
from pydantic import BaseModel
from pydantic import create_model
from pydantic import Field
from pydantic import HttpUrl

# Starboy
from starboy.adapters.kubectl_adapter import KubeConfigAdapter

kube_api = KubeConfigAdapter()


class NestedNaming(BaseModel):
    name: str


class CertAuthority(BaseModel):
    cert_auth: str = Field(..., alias="certificate-authority-data")
    server: HttpUrl


class Cluster(NestedNaming):
    cluster: CertAuthority


class Contexts(NestedNaming):
    context: Optional[Union[
        create_model('InnerContext', cluster=(str, ...), user=(str, ...)),
        dict]] = {}


class UserTokenModel(NestedNaming):
    user: Optional[
        Union[create_model('InnerUser', token=(str, ...), user=(str, ...)),
              dict]] = {}


class ResultConfig(BaseModel):
    apiVersion: Literal["v1"] = "v1"
    clusters: List[Cluster] = []
    contexts: List[Contexts] = []
    current_context: str = ""
    kind: Literal["Config"] = "Config"
    preference: dict = {}
    users: UserTokenModel


def create_service_acc(acc_name: str, namespace: str):
    kns = kube_api.kube_namespace(namespace)
    kns("create", "serviceaccount", acc_name)


def create_cluster_binding(binding_name: str, service_account_name: str,
                           namespace: str):

    kube_api.create("clusterrolebinding", binding_name,
                    "--clusterrole=cluster-admin",
                    f"--serviceaccount={namespace}:{service_account_name}")
    time.sleep(0.5)


def get_sa_secret_name(acc_name: str, namespace: str) -> str:
    kns = kube_api.kube_namespace(namespace)
    kns_get = kns.bake("get")
    name = kns_get(f"serviceaccount/{acc_name}", "-o",
                   "jsonpath='{.secrets[0].name}'").replace("'", "")
    return name


def kube_token(acc_name: str, namespace: str) -> str:
    kns = kube_api.kube_namespace(namespace)
    kns = kube_api.kube_namespace(namespace)
    kns_get = kns.bake("get")
    kns_secret = kns_get.bake("secret")
    token_name: str = get_sa_secret_name(acc_name, namespace)
    token = kns_secret(token_name, "-o",
                       "jsonpath='{.data.token}'").replace("'", "")

    if not token:
        raise ValueError("Token not found")

    return base64.b64decode(token).decode("utf-8")


def get_cert(acc_name: str, namespace: str) -> str:
    kns = kube_api.kube_namespace(namespace)
    kns_get = kns.bake("get")
    kns_secret = kns_get.bake("secret")
    src_name: str = get_sa_secret_name(acc_name, namespace)
    crt = str(kns_secret(src_name, "-o", "jsonpath={.data['ca\.crt']}"))
    time.sleep(0.2)

    return base64.b64decode(str(crt)).decode("utf-8")


def cluster_equal(name: str):
    pass


def create_service_role_and_cluster_binding(service_account_name: str,
                                            namespace: str = "kube-system"):

    ######################################################
    #               Setup Parameters
    ######################################################

    kube_config: KubeConfig = KubeConfig()
    service_account_name = parameterize(service_account_name)
    binding_name = parameterize(f"{service_account_name}-binding")

    ######################################################
    #               Basic Account Setup
    ######################################################

    typer.secho("Creating service account")
    create_service_acc(service_account_name, namespace)
    typer.secho(
        f"Adding service account {service_account_name} to a cluster binding",
        fg="blue",
    )
    create_cluster_binding(binding_name, service_account_name, namespace)

    ######################################################
    #           Get Token And Setup Account
    ######################################################

    token = kube_token(service_account_name, namespace)

    kube_config.set_credentials(service_account_name, token=token)
    kube_api.config("set-context", "--current", "--user", service_account_name)

    certificate = get_cert(service_account_name, namespace)
    current_context = kube_config.current_context()

    ######################################################
    #               Basic Account Setup
    ######################################################

    current_cluster = "gke_astute-impulse-303109_us-central1-c_faas-ambassador-cluster"
    os.environ["CURRENT_CLUSTER"] = str(current_cluster)
    # kubectl config view -o
    # current_address = kube_api.config_view(
    #     "-o",
    #     f"jsonpath=\"{{.clusters[?(@.name == \"${CURRENT_CLUSTER}\"})].cluster.server}}\""
    # )
    # current_address = kube_api.config_view(
    #     "-o", f"jsonpath=\"{.clusters[?(@.name == \"{}\"})].cluster.server}\"")
    # print(current_address)
    # TODO: Maybe come back to this later. You really need to get on the materialized views stuff. This isn't adding any value to your life. Although you hate to repeat the half-finished pattern, this is one place is must slide.
    result = ResultConfig(
        **{
            'clusters': [
                {
                    'cluster': {
                        'certificate-authority-data': certificate,
                        'server': str(current_address),
                    },
                    'name': current_cluster,
                },
            ],
            'contexts': [
                {
                    'context': {
                        'cluster': current_address,
                        'user': service_account_name,
                    },
                    'name': current_context,
                },
            ],
            'current-context':
            current_context,
        })
    # user_dict = Addict()
    # user_dict.name = current_cluster
    # user_dict.user.token = token

    # debug(result)

    pass
