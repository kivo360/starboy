# Standard Library
import time
from typing import Optional

import typer

# Development Tools
import pathlib
from pathlib import Path

# Starboy
from starboy import api
from starboy import models
from starboy import repos
from starboy import steps
from starboy.devops.kube.service_account import \
    create_service_role_and_cluster_binding

kube_app = typer.Typer()
app = typer.Typer()
app.add_typer(
    kube_app,
    name="kube",
    help=
    "Multiple automated kubernetes commands. Based on circumstances we've come across."
)
# HOME_FOLDER = pathlib.Path().home()


@app.command('autopush')
def auto_push(mount: Path = typer.Argument(...)):
    """A single command that aims to install everything on the computer."""
    print(f"Going to save the state to the mounted folder {mount}")
    analyzed = steps.get_faas_resources(mount)
    local_reqs = api.DevState(**analyzed)  # type: ignore
    git_state = api.GithubState()
    # This automatically runs everything it needs to for a cohesive github task.
    repos.GithubController(origin=local_reqs, target=git_state)


@kube_app.command('sa_setup')
def init_cluster_binding(acc_name: str = typer.Option("", prompt=True),
                         namespace: str = typer.Option(default="kube-system")):
    """Create a new kubernetes service account and a cluster role as well. """
    if not acc_name:
        typer.echo("Service account name cant be blank")
    create_service_role_and_cluster_binding(acc_name, namespace)
