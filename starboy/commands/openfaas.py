# Standard Library
from contextlib import suppress
import os
import shutil

import sh

# Development Tools
from loguru import logger
from pathlib import Path

# Pydantic and Serialization
from pydantic import BaseModel
from pydantic import validate_arguments

faas_cli = sh.Command("faas-cli")
faas_cli_help = faas_cli.bake("-h")
faas_cli_new_help = faas_cli.bake("new", "-h")


class FaasFiles(BaseModel):
    path: Path = Path().cwd()
    name: str

    @property
    def file(self):
        return self.path / f"{self.name}.yml"

    @property
    def fldr(self):
        return self.path / self.name


def create_faas(name: str, lang: str, *args, path: Path = Path().cwd()):
    with suppress(Exception):
        fast = FaasFiles(path=path, name=name)
        faas_cli.new(name, "--lang", lang, *args, _cwd=path)


def remove_faas(name: str, path: Path):
    with suppress(Exception):
        fast = FaasFiles(path=path, name=name)
        if fast.file.exists() and fast.file.is_file():
            os.remove(fast.file)
        if fast.fldr.exists() and fast.fldr.is_dir():
            shutil.rmtree(fast.fldr)
