from __future__ import annotations

# Standard Library
import abc
from contextlib import suppress
from datetime import datetime
from operator import attrgetter as attrgt
import sys
import time
from typing import Any, cast, Dict, List, Optional, Type, Union

from github import Github
from github import InputGitTreeElement
# from github import Organization
from github.Organization import Organization
from github.Repository import Repository
from inflection import underscore

# Development Tools
from addict import Addict
from devtools import debug
import devtools as deev
from loguru import logger
from pathlib import Path
from pathlib import PurePath

# Pydantic and Serialization
from pydantic import BaseModel
from pydantic import BaseSettings
from pydantic import DirectoryPath
from pydantic import Field
from pydantic import FilePath
from pydantic.class_validators import root_validator
from pydantic.class_validators import validator
from pydantic.types import NoneStr

# Starboy
from starboy.steps import get_faas_root

CompareDict = Dict[str, 'Comparators']

# from operator import
MONOREPO_KEYS = {"serverless", "services", "covered"}


def get_or_create_main(fn_name: str, org: Organization):
    # Create a function that returns the create file.
    fn_repo = org.get_repo(fn_name)
    capn = fn_name.capitalize()
    main_branch = None
    with suppress(Exception):
        main_branch = fn_repo.get_branch("main")
        return main_branch, fn_repo
    fn_repo.create_file("README.md", f"First commit for {fn_name}",
                        f"# Serverless Function {capn} \nFile Content")
    main_branch = fn_repo.get_branch("main")
    return main_branch, fn_repo


def create_new_commit(fn_layer: str, input_el: List[InputGitTreeElement],
                      org: Organization):
    _mbranch, _repo = get_or_create_main(fn_layer, org=org)
    _head_ref = list(_repo.get_git_refs())[0]
    latest_commit = _repo.get_git_commit(_head_ref.object.sha)
    base_tree = _repo.get_git_tree(_head_ref.object.sha)
    new_tree = _repo.create_git_tree(input_el, base_tree)
    new_commit = _repo.create_git_commit(
        message=f"Updating the faas function {fn_layer} ... {datetime.now()}",
        parents=[latest_commit],
        tree=new_tree)
    _head_ref.edit(sha=new_commit.sha, force=False)
    return _head_ref, new_commit, _repo


class UnreasonFlex(BaseModel):
    """
    UnreasonFlex

    Unreasonable Flexibility

    Just a BaseModel that has the config setup for easily entering in information of different kinds.

    """
    class Config:
        arbitrary_types_allowed = True
        extras = 'allowed'


class GithubSettings(BaseSettings):
    # auth_key: str
    personal_token: str = Field(env='GITHUB_PERSONAL_ACCESS')
    faas_prefix: str = "faas_memez_"
    org_name: str = "bodhi-sync"


class ServerlessFn(BaseModel):
    config_file: FilePath
    fn_dir: DirectoryPath
    input_elements: List[InputGitTreeElement] = []

    class Config:
        arbitrary_types_allowed = True

    @root_validator
    def gather_files(cls, values: Dict[str, Any]):
        loc_dict = Addict(**values)
        loc_dict.input_elements = []
        _details = loc_dict.fn_dir
        for cont in _details.rglob("*"):
            is_file = cont.is_file()
            if is_file:
                rel_path = cont.relative_to((get_faas_root()))
                rel_path_str = str(rel_path)
                print(rel_path_str)
                file_content = cont.open().read()
                in_element = InputGitTreeElement(path=rel_path_str,
                                                 mode='100755',
                                                 type='blob',
                                                 content=file_content)
                loc_dict.input_elements.append(in_element)
        file_content = loc_dict.config_file.open().read()
        in_element = InputGitTreeElement(path=loc_dict.config_file.name,
                                         mode='100755',
                                         type='blob',
                                         content=file_content)
        loc_dict.input_elements.append(in_element)

        return loc_dict.to_dict()


class MonoItem(UnreasonFlex):
    name: str
    path: Optional[Union[Path, str]]


class MonoState(UnreasonFlex, abc.ABC):
    type_name: str
    pre: bool = False
    remote: bool = False
    serverless: List[MonoItem] = []
    services: List[MonoItem] = []
    covered: List[MonoItem] = []

    @property
    def key_states(self) -> Dict[str, Any]:
        return self.dict(include=MONOREPO_KEYS)

    @property
    def key_cpy(self) -> 'MonoState':
        return self.copy(include=MONOREPO_KEYS)


class Rule(UnreasonFlex):
    opt_name: str

    @abc.abstractmethod
    def run(self, rinput) -> None:
        pass


RuleInsts = List[Rule]
RuleTypes = List[Type[Rule]]


class RuleProcessor(UnreasonFlex):
    """
    StateProcessor

        Takes the current state and determines what to do next. Goes along side the comparison class.

        Resembles pants builds runner class. Rules will be coded in directly for time's sake.

    """
    # name: Optional[str] = None  # gets name of specific processors
    # placeholder_rules: List[Type[Rule]] = [CreateRepos]
    rules: RuleTypes = []
    rule_dict: Dict[str, Rule] = {}
    parent: StateController

    def get_rule(self, name: str) -> Optional[Rule]:
        return self.rule_dict.get(name, None)

    @abc.abstractmethod
    def run(self, *args, **kwargs) -> None:
        """
        run Runs a rule set to be used with the controller.

        Will implement it's own rule running logic depending on what subclasses it.
        The process will vary, though the outcome will be the same.

        """


class StateController(UnreasonFlex):
    origin: Union[MonoState, Dict[str, Dict[str, List[Dict[str, str]]]],
                  Dict[str, Any]]
    target: MonoState
    comparisons: List['Comparators'] = []
    processors: Optional['RuleProcessor'] = None

    unsynced: CompareDict = {}

    def __init__(self, **data: Any) -> None:  # type: ignore
        super().__init__(**data)
        # self.find_unsynced()
        self.comparisons = self.add_parent()
        self.run_comparison()

    def add_parent(self) -> List[Comparators]:
        comp_val = []
        for com in self.comparisons:
            com.parent = self
            comp_val.append(com)
        return comp_val

    def unsync_by(self, opt: str) -> Optional[Comparators]:
        return self.unsynced.get(opt, None)

    def run_comparison(self) -> CompareDict:
        # logger.debug("Starting mass gap comparisons ...")
        for comp in self.comparisons:
            comp_out = comp.compare()
            opt = comp_out.operation_name
            if opt is None:
                continue
            _core = comp_out.key_cpy
            self.unsynced[opt] = _core  # type: ignore
        logger.debug("Completed running comparisons ...")
        return self.unsynced

    def is_unsync(self, name: str) -> bool:
        return name in self.unsynced


class DevState(MonoState):
    type_name: str = "local"
    pre: bool = True


class RemoteState(MonoState):
    remote: bool = True
    caller: Any = None

    def __init__(self, **data: Any) -> None:  # type: ignore
        super().__init__(**data)
        self.load_settings()

    @abc.abstractmethod
    def load_settings(self) -> None:
        raise NotImplementedError


# -------------------------------------------------------------
# --------------------------- Github --------------------------
# -------------------------------------------------------------

git_set = GithubSettings()


def is_faas(repo_name: str) -> bool:
    return repo_name.startswith(git_set.faas_prefix)


def to_git_faas(fn_name: str) -> str:
    return f"{git_set.faas_prefix}{fn_name}"


def get_faas_repos(repo_list: List[Repository]) -> List[MonoItem]:

    faas_repos: List[MonoItem] = []
    for repo in repo_list:
        fn_name = repo.name
        if is_faas(fn_name):
            faas_repos.append(MonoItem(name=fn_name, path=repo.full_name))
    return faas_repos


def name_in(item_list: List[MonoItem], name: str) -> bool:
    for item in item_list:
        if item.name == name:
            return True
    return False


class MissingRemote(MonoState):
    remote: bool = True
    type_name: str = "missing"


class GithubState(RemoteState):
    type_name: str = "github"
    settings: GithubSettings = GithubSettings()
    api: Github = None  # type: ignore

    @validator('api', pre=True, always=True)
    def valid_api(cls, v: Optional[Github], values: Dict[str, Any],
                  **kwargs) -> Github:
        settings: GithubSettings = values.get("settings")  # type: ignore
        return v or Github(settings.personal_token)

    # TODO: Deduplicate code here.

    @property
    def org(self) -> Organization:
        """
        org Get organization

        Returns:
            Organization: A organization github object.
        """
        return self.api.get_organization(self.settings.org_name)

    @property
    def org_repos(self) -> List[Repository]:
        return list(self.org.get_repos())

    def load_settings(self):
        self.serverless = get_faas_repos(self.org_repos)


class Comparators(MonoState):
    remote: bool = True
    type_name: str = "comparers"
    operation_name: Optional[str] = None

    parent: Optional['StateController'] = None

    @validator('operation_name', pre=True, always=True)
    def set_ts_now(cls, v: NoneStr) -> str:
        return v or underscore(cls.__name__)  # type: ignore

    @abc.abstractmethod
    def compare(self) -> 'Comparators':
        raise NotImplementedError


class RepoNotExist(Comparators):
    def compare(self) -> 'Comparators':
        org = self.parent.origin  # type: ignore
        trg = self.parent.target  # type: ignore
        tckey = trg.key_states
        for cat_name, val in org.key_cpy:
            item_list: List[Any] = cast(List[Any], tckey.get(cat_name))
            for itm in val:
                iname: str = itm.name
                if not name_in(item_list, itm.name):
                    assets: List[MonoItem] = attrgt(cat_name)(self)
                    assets.append(MonoItem(name=iname))
                    setattr(self, cat_name, assets)
        MAIN_KEYS = MONOREPO_KEYS | {"operation_name", "type_name"}
        outcome = self.copy(include=MAIN_KEYS)
        return outcome


RepoNotExist.update_forward_refs()


class RepoDataChange(Comparators):
    def compare(self) -> 'Comparators':
        org = self.parent.origin  # type: ignore
        trg = self.parent.target  # type: ignore
        tckey = trg.key_states
        logger.error(tckey)
        # logger.warning(tckey)
        for cat_name, val in org.key_cpy:
            # logger.debug((cat_name, val))
            setattr(self, cat_name, val)
        MAIN_KEYS = MONOREPO_KEYS | {"operation_name", "type_name"}
        outcome = self.copy(include=MAIN_KEYS)
        return outcome


RepoDataChange.update_forward_refs()


class GitRules(Rule):
    opt_name: str
    api: Optional[Github] = None
    settings: GithubSettings = GithubSettings()

    @property
    def org(self) -> Organization:
        """
        org Get organization

        Returns:
            Organization: A organization github object.
        """
        if self.api is None:
            raise AttributeError("The github api was not found.")
        return self.api.get_organization(self.settings.org_name)

    @property
    def org_repos(self) -> List[Repository]:
        return list(self.org.get_repos())

    def load_settings(self):  # type: ignore
        self.serverless = get_faas_repos(self.org_repos)

    @abc.abstractmethod
    def run(self, rinput: Any) -> None:
        pass


# ---------------------------------------------------------------------------------------
# ------------------------- My direct github command goes here ---------------------------
# ---------------------------------------------------------------------------------------


def create_github_repo(org_api: Organization, repo_name: str):
    logger.info(f"Attempting to create repository  for {repo_name}")
    try:
        org_api.create_repo(
            to_git_faas(repo_name),
            auto_init=True,
            description=
            "Tempor qui velit enim cupidatat excepteur ullamco do cupidatat commodo velit duis."
        )
    except Exception as e:
        logger.debug(e)


def remove_github_repo(org_api: Organization, repo_name: str):
    logger.error(f"Attempting to delete repository for {repo_name}")
    # logger.info(repo_name)
    try:
        org_api.get_repo(to_git_faas(repo_name)).delete()
    except Exception as e:
        logger.error(e)


class CreateRepos(GitRules):
    opt_name: str = 'repo_not_exist'

    def is_runnable(self) -> bool:
        if self.api is None:
            return False
        elif self.settings is None:
            return False
        return True

    def run(self, rinput: Union[Comparators]):
        logger.info(f"Starting to run {self.__class__.__name__}")
        if not self.is_runnable():
            return
        removal_list = []
        deev.debug(rinput)
        for _, item_details in rinput.dict().items():
            if not isinstance(item_details, list):
                continue
            if len(item_details) <= 0:
                continue
            item: MonoItem
            for item in item_details:
                is_any = any(isinstance(x, MonoItem) for x in item_details)
                if not is_any:
                    continue

                removal_list.append(item.name)
                create_github_repo(self.org, item.name)

        time.sleep(2)
        for rm_name in removal_list:
            remove_github_repo(self.org, rm_name)


"""
    class GithubRepoFiles(GitRules):
        opt_name: str = 'repo_files'

        def is_runnable(self) -> bool:
            if self.api is None:
                return False
            elif self.settings is None:
                return False
            return True

        def run(self, rinput: Union[Comparators]):
            logger.info(
                f"Starting to run {self.__class__.__name__.capitalize()} if possible ..."
            )
            if not self.is_runnable():
                return
            logger.debug("Adding all of the necessary files and labels.")
            # removal_list = []
            # deev.debug(rinput)
            # for _, item_details in rinput.dict().items():
            #     if not isinstance(item_details, list):
            #         continue
            #     if len(item_details) <= 0:
            #         continue
            #     item: MonoItem
            #     # logger.success(item_details)
            #     for item in item_details:
            #         is_any = any(isinstance(x, MonoItem) for x in item_details)
            #         if not is_any:
            #             continue

            #         # if not item.
            #         # if isinstance(item, MonoItem)
            #         removal_list.append(item.name)
            #         create_github_repo(self.org, item.name)

            # time.sleep(10)
            # for rm_name in removal_list:
            #     remove_github_repo(self.org, rm_name)

"""


class GithubProcessor(RuleProcessor):

    # api: Github
    # CreateRepos,
    rules: RuleTypes = [CreateRepos]
    parent: StateController

    def __init__(__pydantic_self__, **data: Any) -> None:  # type: ignore
        super().__init__(**data)

    @root_validator
    def git_root(cls, values):
        local_values = Addict(**values)
        github_rules = local_values.rules
        g_dict = local_values.rule_dict
        parent = local_values.parent
        local_api = parent.api
        local_settings = parent.settings
        rulez: RuleTypes = []
        for rule in github_rules:
            new_rule = rule(api=local_api, settings=local_settings)
            g_dict[new_rule.opt_name] = new_rule
            rulez.append(new_rule)
        local_values.rule_dict = g_dict
        local_values.rules = rulez
        values = local_values.to_dict()
        # debug(values)
        return values

    def run(self, *args, **kwargs) -> None:
        # usync = compared
        logger.warning("Ruuning the github processor successfully")
        compared = Addict(self.parent.unsynced)

        if not compared:
            return
        # logger.info(self.rule_dict)
        for key, rule in self.rule_dict.items():

            if key not in compared:
                continue
            logger.warning(f"Checking for extra loops: {key}")
            rule.run(compared[key])
            # debug(rule, )

        deev.debug(list(compared.keys()))


class GithubController(StateController):
    target: GithubState
    comparisons: List['Comparators'] = [RepoNotExist(), RepoDataChange()]
    processors: Optional['RuleProcessor'] = None
    settings: GithubSettings = GithubSettings()
    api: Github = None  # type: ignore

    def __init__(self, **data: Any) -> None:  # type: ignore
        super().__init__(**data)
        self.valid_processors()
        if self.processors is not None:
            self.processors.run(compared=self)

    @validator('api', pre=True, always=True)
    def valid_api(cls, v: Optional[Github], values: Dict[str, Any],
                  **kwargs) -> Github:
        settings: GithubSettings = values.get("settings")  # type: ignore
        return v or Github(settings.personal_token)

    def valid_processors(self, *args, **kwargs) -> None:
        self.processors = GithubProcessor(api=self.api, parent=self)

    @property
    def org(self) -> Organization:
        """
        org Get organization

        Returns:
            Organization: A organization github object.
        """
        return self.api.get_organization(self.settings.org_name)

    @property
    def org_repos(self) -> List[Repository]:
        return list(self.org.get_repos())

    def load_settings(self):  # type: ignore
        self.serverless = get_faas_repos(self.org_repos)


GithubController.update_forward_refs()


def main() -> None:
    local_state = {
        "serverless": [{
            "name": "chatbot",
            "path": "/tmp/turtule"
        }, {
            "name": "scanner",
            "path": "/tmp/scanner"
        }, {
            "name": "chico",
            "path": "/tmp/covered"
        }],
        "services": [],
        "covered": [],
    }
    local_state = DevState(**local_state)  # type: ignore
    git_state = GithubState()
    # logger.info(DevState(**local_state))
    git_control = GithubController(origin=local_state, target=git_state)
    # git_control.add_parent()
    # # deev.debug(git_control.compars[0])


if __name__ == "__main__":
    # TODO: Refactor to have all github accessible objects either inherit the same class or take the StateController as the parent.
    # The state controller wouuld be most consistent between all classes.
    main()
