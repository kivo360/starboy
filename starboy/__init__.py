# Development Tools
import pathlib

# Pydantic and Serialization
import pydantic

from .cargo_model import CargoModel
from .diffcheck import init_store as init_storage


def custom_serialize(obj):
    if isinstance(obj, pathlib.Path):
        return obj.absolute().as_posix()


__version__ = '0.1.0'
pydantic.json.ENCODERS_BY_TYPE[pathlib.PosixPath] = custom_serialize
pydantic.json.ENCODERS_BY_TYPE[pathlib.WindowsPath] = custom_serialize
