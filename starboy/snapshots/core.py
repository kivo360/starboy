from __future__ import annotations

# Standard Library
import itertools
import os
from typing import Any, Dict, List, Optional, Union

from toolz import concat
from watchdog.utils.dirsnapshot import DirectorySnapshot
from watchdog.utils.dirsnapshot import DirectorySnapshotDiff

# Development Tools
from devtools import debug
from loguru import logger
from pathlib import Path
from pathlib import PurePath

# Pydantic and Serialization
import json
from pydantic import BaseModel
from pydantic import DirectoryPath
from pydantic import utils
from pydantic import validate_arguments

# Starboy
from starboy import utils
from starboy.types import SnapField

# from starboy.commands import remove_faas
# from starboy.models import PathTraversal
# from starboy.path_editor import FaasManager
# from starboy.path_editor.manager.faas import FaasManager
# from starboy.snapshots import TotalChange

DictPaths = Dict[str, Union[Path, Any]]


# Use a mediator to get the information we need to
class ChangeStats(BaseModel):
    prefix: str
    created: List[Path] = []
    deleted: List[Path] = []
    modified: List[Path] = []
    moved: List[Path] = []

    def __init__(self, snap_diff: DirectorySnapshotDiff, **data: Any) -> None:
        super().__init__(**data)
        values = self.dict()
        prefix = values.pop("prefix", None)
        for key in values.keys():
            dff = getattr(snap_diff, f"{prefix}_{key}", [])
            updated = values[key] + dff
            setattr(self, key, updated)


_existing = [
    "created",
    "modified",
    "moved",
]


class TotalChange(BaseModel):
    # changes: DictPaths = {}
    files: DictPaths = {}
    folders: DictPaths = {}

    def __init__(self, snap_diff: DirectorySnapshotDiff) -> None:
        dir_changes = ChangeStats(snap_diff, prefix="dirs")
        file_changes = ChangeStats(snap_diff, prefix="files")
        _folders = dir_changes.dict(exclude={'prefix'})
        _files = file_changes.dict(exclude={'prefix'})
        super().__init__(**{"files": _files, "folders": _folders})

    @property
    def exe_folds(self):
        exports = dict(
            (k, self.folders[k]) for k in _existing if k in self.folders)
        pre_made = itertools.chain(*exports.values())
        return map(lambda x: PurePath(x), pre_made)

    @property
    def exe_files(self):
        exports = dict(
            (k, self.files[k]) for k in _existing if k in self.files)
        # PurePath
        pre_made = itertools.chain(*exports.values())
        return map(lambda x: PurePath(x), pre_made)

    @property
    def merged_paths(self):
        return list(concat([self.exe_files, self.exe_folds]))


class SnapshotEncoder(BaseModel):
    snap_set: List[SnapField] = []

    def create_snapset(self, dir_snap: DirectorySnapshot):
        stat_info = dir_snap._stat_info
        # inode_info = dir_snap._inode_to_path
        for file_path, os_stat in stat_info.items():
            inode = dir_snap.inode(file_path)
            self.snap_set.append(
                SnapField(file_path=file_path,
                          os_stats=os_stat,
                          inode_id=inode))

    def rebuild_snapshot(self):
        init_snap = DirectorySnapshot(Path())
        _stat_info = {}
        _inode_to_path = {}
        for snap_field in self.snap_set:
            _stat_info[
                snap_field.file_path] = snap_field.os_stats.get_os_stat()
            _inode_to_path[snap_field.inode_id] = snap_field.file_path

        init_snap._stat_info = _stat_info
        init_snap._inode_to_path = _inode_to_path
        return init_snap


class SnapshotWrapper:
    @validate_arguments
    def __init__(self,
                 path: Union[Path] = Path().cwd(),
                 is_skip: bool = False) -> None:

        self._snapshot: DirectorySnapshot = None
        self._encoder = SnapshotEncoder()
        if not is_skip:
            _snapshot = DirectorySnapshot(path)
            self._encoder.snap_set = []
            self._encoder.create_snapset(_snapshot)
            self._snapshot = self._encoder.rebuild_snapshot()
        else:
            dstat = path.lstat()
            self._encoder.snap_set = [
                SnapField(file_path=path,
                          os_stats=dstat,
                          inode_id=(dstat.st_ino, dstat.st_dev))
            ]

    @property
    def dir_info(self):
        """
        dir_info Gets the internal directory information.

        Returns:
            List[SnapField]: A list of snapshot field objects. The they're pydantic models for stability's sake.
        """
        return self._encoder.snap_set

    def json(self):
        return self._encoder.json()

    def load(self, json_values: str):
        """
        load Take the json values from a source and convert it into a Directory Snapshot.

        Args:
            json_values (str): The input values of the class we have.   

        Returns:
            [type]: [description]
        """
        self._encoder.parse_raw(json_values)
        self._snapshot = self._encoder.rebuild_snapshot()
        return self

    @validate_arguments
    def load_path(self, path: Path):
        content = path.open().read()
        self.load(content)
        return self

    def __len__(self):
        return len(self._encoder.snap_set)

    # def

    def __str__(self) -> str:
        values_str = utils.truncate(str(self._encoder.dict().get(
            "snap_set", None)),
                                    max_len=50)
        return f"SnapshotWrapper({values_str})"

    def __repr__(self) -> str:
        # snap_set
        return self.__str__()

    @validate_arguments(config=dict(arbitrary_types_allowed=True))
    def __sub__(self, previous_dirsnap: Union[DirectorySnapshot,
                                              'SnapshotWrapper']):
        """Allow subtracting a DirectorySnapshot object instance from
        another.

        :returns:
            A :class:`DirectorySnapshotDiff` object.
        """
        prev: DirectorySnapshot = None
        if isinstance(previous_dirsnap, self.__class__):
            prev = previous_dirsnap._snapshot
        else:
            prev = previous_dirsnap
        return TotalChange(DirectorySnapshotDiff(prev, self._snapshot))
