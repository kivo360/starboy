# Standard Library
from io import TextIOWrapper
import os
from typing import Any, Dict, List, Optional, Tuple, Type

from dictdiffer import diff
from dictdiffer import dot_lookup
from dictdiffer import patch
from dictdiffer import revert
from dictdiffer import swap
from dictdiffer.utils import get_path

# Development Tools
from loguru import logger
from pathlib import Path
from pathlib import PurePath

DictDiff = List[Tuple[str, str, Tuple[Any]]]
DictStrAny = Dict[str, Any]


def file_overwrite(file_path: Path, content: str):
    with file_path.open("w+") as fwrap:
        fwrap.seek(0)
        fwrap.write(content)
        fwrap.truncate()


def add_key(origin: dict, key: str) -> dict:
    _placeholder = {}
    _placeholder[key] = origin
    return _placeholder


def diff_dict_gen(diff_result: DictDiff) -> Dict[str, Any]:
    def blank_node(key: str):
        output: Dict[str, Any] = {}
        dsplit = key.split(".")
        if len(dsplit) < 2:
            # NOTE: If there's only the key we just returned the updated object.
            return add_key(output, key)
        for key in reversed(dsplit):
            output = add_key(output, key)
        return output

    def empty_aggregate(_diff_res: DictDiff):
        combined = {}
        for action, node, changes in _diff_res:

            combined.update(blank_node(node))
        return combined

    # NOTE: Going to use the difference of repositories and this dict to determine what needs to get disabled
    agg_target = empty_aggregate(diff_result)
    return patch(diff_result, agg_target)


def diff_dict(first: dict, second: dict) -> Dict[str, Any]:
    """Get a dictionary that has difference between file names.

    Args:
        first (dict): The original dictionary.
        second (dict): The newly loaded dictonary

    Returns:
        Dict[str, Any]: All of the remaining values
    """
    dd = diff(first=first, second=second)
    return diff_dict_gen(dd)


def path_is_parent(parent_path: Path, child_path: Path) -> bool:
    # Smooth out relative path names, note: if you are concerned about symbolic links, you should use os.path.realpath too
    parent_path = os.path.abspath(parent_path)
    child_path = os.path.abspath(child_path)

    # Compare the common path of the parent and child path with the common path of just the parent path. Using the commonpath method on just the parent path will regularise the path name in the same way as the comparison that deals with both paths, removing any trailing path separator
    return os.path.commonpath([parent_path]) == os.path.commonpath(
        [parent_path, child_path])


def is_valid_path(faas_obs: DictStrAny, item: Path):
    """ 
        We're checking over our validated top-level list to determine if the changed file is worth placing into a new serverless function. Using it repeatedly to verify each file and directory. If an individual file is found inside of the sets we introduce we can say it's valid. If it's found in a directory we remove it to our add list and just select the directory instead.
    """
    # logger.info((faas_obs, item))
    for key, paths in faas_obs.items():
        if not key or not paths:
            continue
        cont: Path = paths.get("content")
        config: Path = paths.get("config")

        # is_parent = path_is_parent(cont, item)
        if path_is_parent(cont, item):
            return (True, key, True, PurePath(cont))
        elif item == cont:
            return (True, key, True, PurePath(cont))
        elif item == config:
            return (True, key, False, PurePath(config))

    return (False, None, False, None)


def rm_root(_root: Path, pathies: List[Path]):
    return list(filter(lambda y: y != _root, pathies))


def filter_faas_candidates(faas_root: Path, total_change: list,
                           observation: dict):
    # TODO: Make this function generic the second time around.
    resp = set()
    if not observation or not total_change:
        return set()
    for candidate in rm_root(faas_root, total_change):  # type: ignore
        is_valid, fn_name, is_bundle, parent_dir = is_valid_path(
            observation, candidate)
        if is_valid and not is_bundle:
            # print(candidate)
            resp.add((fn_name, candidate))
        elif is_valid and is_bundle:
            resp.add((fn_name, parent_dir))

        # print(is_valid, fn_name)
    return resp
