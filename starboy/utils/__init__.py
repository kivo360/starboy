# Standard Library
from typing import Union

from .dicttoolzz import delete_keys_from_dict as remove_dict_keys
from .fileutilz import diff_dict
from .fileutilz import file_overwrite
from .fileutilz import filter_faas_candidates
from .msg import rainbow_log
from .validation.faas import is_faas
from .validation.faas import is_parent
from .validation.faas import is_parent_set
from .validation.faas import matched_internal
from .validation.paths import verify_path


def truncate(v: Union[str], *, max_len: int = 80) -> str:
    """
    Truncate a value and add a unicode ellipsis (three dots) to the end if it was too long
    """

    if isinstance(v, str) and len(v) > (max_len - 2):
        # -3 so quote + string + … + quote has correct length
        return (v[:(max_len - 3)] + '…').__repr__()
    try:
        v = v.__repr__()
    except TypeError:
        v = v.__class__.__repr__(v)  # in case v is a type
    if len(v) > max_len:
        v = v[:max_len - 1] + '…'
    return v
