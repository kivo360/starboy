import yaml
# from genson import SchemaBuilder
from pathlib import Path
from pydantic import BaseModel, AnyHttpUrl, Field, ValidationError
from contextlib import suppress
from typing import Dict, Any, Type, List, Tuple, Optional
from loguru import logger

StrDictAny = Dict[str, Any]


def delete_keys_from_dict(dictionary: Dict[str, Any], keys: list):
    for key in keys:
        with suppress(KeyError):
            # dictionary.pop('title', None)
            del dictionary[key]
    for value in dictionary.values():
        if isinstance(value, dict):
            delete_keys_from_dict(value, keys)


class Resource(BaseModel):
    cpu: Optional[str]
    memory: Optional[str]


class Template(BaseModel):
    name: str
    source: Optional[AnyHttpUrl]


class ConfigOptions(BaseModel):
    templates: Optional[List[Template]]
    _copy: Optional[List[Path]] = Field(alias="copy")


class FaaSFunc(BaseModel):
    lang: str
    handler: str
    image: str
    skip_build: Optional[bool]
    build_options: Optional[List[str]]
    build_args: Optional[Dict[str, str]]
    environment: Optional[Dict[str, Any]]
    environment_file: Optional[List[str]]
    secrets: Optional[List[str]]
    readonly_root_filesystem: Optional[bool]
    constraints: Optional[List[str]]
    labels: Optional[Dict[str, str]]
    annotations: Optional[Dict[str, str]]
    limits: Optional[Resource]
    requests: Optional[Resource]
    configuration: Optional[ConfigOptions]


class Provider(BaseModel):
    name: str
    gateway: AnyHttpUrl


class FaasYamlConfig(BaseModel):
    provider: Provider
    functions: Dict[str, FaaSFunc]

    class Config:
        @staticmethod
        def schema_extra(schema: Dict[str, Any],
                         model: Type['FaasYamlConfig']) -> None:
            for key, prop in schema.get('properties', {}).items():
                prop.pop('title', None)


def is_faas_yaml_detail(_faas_yaml: StrDictAny) -> bool:
    """Uses pydantic to check if this is a model in detail.

    Args:
        _faas_yaml (StrDictAny): A dictionary with the yaml config. 

    Returns:
        bool: Returns true if it's valid. False if it's invalid.
    """
    try:
        FaasYamlConfig(**_faas_yaml)
        return True
    except ValidationError as e:
        logger.exception(e)
        return False