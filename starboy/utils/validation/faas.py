# Standard Library
import itertools
import os
from pathlib import Path
from typing import List

import yaml
from loguru import logger
from toolz.functoolz import curry

from .model_based import is_faas_yaml_detail


def is_empty(_item_empty: Path) -> bool:
    return not any(_item_empty.iterdir())


def is_faas(file: Path) -> bool:
    """Checks if the yaml file has the fields we need. 
    
    `functions` and `provider`

    Args:
        file (Path): The yaml file we're analyzing.

    Returns:
        bool: True if this yaml file is a faas file.
    """
    if not file.exists(): return False
    if not os.path.getsize(file) > 0: return False
    try:
        yml_body: dict = yaml.load(file.open().read(), Loader=yaml.FullLoader)
        return is_faas_yaml_detail(**yml_body)
    except:
        return False


def matched_internal(_file: Path):
    # resv = _file.resolve()
    rparent = _file.parent
    # logger.info(rparent)
    yaml_stem: str = _file.stem
    loc_parent_folders = [
        _folder for _folder in rparent.iterdir() if _folder.is_dir()
    ]
    for local_folder in loc_parent_folders:
        if local_folder.stem == yaml_stem:
            if not is_empty(local_folder):
                yield local_folder


def wrap_match_str(name: str, ext: str = "yaml"):
    return f"**/{name}/**/*.{ext}"


def is_parent(child_path: Path, parent_name: str, ext: str = "yaml") -> bool:
    wrpr: str = wrap_match_str(parent_name, ext)
    return child_path.resolve().match(wrpr)


@curry
def is_parent_set(child_path: Path,
                  folder_names: List[str] = [],
                  exts: List[str] = ["yaml"]) -> bool:
    """Checks a file to see if it has one of the matching parents

    Args:
        child_path (Path): The file that we're checking.
        flr_names (List[str], optional): The folder names we want to check. Defaults to [].
        ext (List[str], optional): The extensions we want to check. Defaults to ["yaml"].

    Raises:
        AttributeError: If the folder name is missing.
        AttributeError: If one of the extensions are missing.

    Returns:
        bool: True if there is a match. 
    """
    if not folder_names:
        raise AttributeError("The parent_names you seek must not be empty.")
    if not exts:
        raise AttributeError("Must have at least one extension in a list")
    for _pnt, _ext in itertools.product(folder_names, exts):
        is_pnt = is_parent(child_path, _pnt, _ext)
        if is_pnt: return True
    return False
