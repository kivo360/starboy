# Standard Library
from typing import Union

# Development Tools
from pathlib import Path

# Pydantic and Serialization
from pydantic import validate_arguments


@validate_arguments
def verify_path(path_input: Path) -> Path:
    return path_input
