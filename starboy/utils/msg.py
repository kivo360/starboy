# Standard Library
import random

# Development Tools
from loguru import logger


def rainbow_log(msg):
    local_log = random.choice[logger.info, logger.debug, logger.success,
                              logger.error, logger.critical]
    local_log(msg)
