from contextlib import suppress
# from collections import MutableMapping
from typing import Any, Dict


def delete_keys_from_dict(dictionary: dict, keys: list):
    for key in keys:
        with suppress(KeyError):
            del dictionary[key]
    for value in dictionary.values():
        if isinstance(value, dict):
            delete_keys_from_dict(value, keys)