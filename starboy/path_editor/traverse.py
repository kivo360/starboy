from starboy.path_editor.utils import appended_path
from typing import List
from pathlib import Path


def find_match_all(filter_list: List[str], current_dir: Path):
    curr = current_dir
    for _ in range(6):
        x = curr.glob("*")
        y = filter(lambda a: a.is_file(), x)
        z = filter(lambda b: b.name in filter_list, y)
        z = list(z)
        if len(z) >= len(filter_list):
            return curr
        curr = curr.parent
    raise AttributeError("Could not find the project root folder.")


def get_pants_toml(parent: Path) -> Path:

    return appended_path(parent, "pants.toml")