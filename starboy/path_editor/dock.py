"""
    All python code regarding docker will go here.


    The aim is to control the docker environemt.
"""
"""
docker run 
  -p 6379:6379
  -v /home/user/data:/data \
  redislabs/redismod \
  --loadmodule /usr/lib/redis/modules/rebloom.so \
  --dir /data
"""
import docker
import pathlib as pl
import tempfile


def install_redis_docker():
    client = docker.from_env()

    key = "redis-session"
    temp = tempfile.gettempdir()
    folder_name = f"{temp}/{key}/"

    p = pl.Path(folder_name)

    p.mkdir(parents=True, exist_ok=True)

    vols = {str(p): dict(bind='/data', mode='rw')}

    client.containers.run('redislabs/redismod',
                          ports={'6379/tcp': 6379},
                          volumes=vols,
                          detach=True)
