# Standard Library
from itertools import chain
from typing import Any, Dict, Iterable, Tuple

# Development Tools
from loguru import logger
from pathlib import Path

# Starboy
from starboy.path_editor.manager.abstracts import ProjectManagerAbs
from starboy.utils import is_faas
from starboy.utils import is_parent_set
from starboy.utils import matched_internal


class FaasManager(ProjectManagerAbs):
    def __init__(self, lang_root_path: Path) -> None:
        super().__init__(lang_root_path)
        # Add other stuff (PC Culture Bro) here

    @property
    def observed(self):
        return self.watchable_functions()

    def __root_glob(self, glob_str: str) -> Iterable[Path]:
        return self.root_path.rglob(glob_str)

    def list_subspaces(self) -> Iterable[Any]:
        yamas_check = is_parent_set(folder_names=["template"],
                                    exts=["yml", "yaml"])
        all_yml = chain(*[self.__root_glob(x) for x in ["*.yml", "*.yaml"]])
        _file: Path

        for _file in all_yml:
            if not yamas_check(_file) and not is_faas(_file):

                for _mfld in matched_internal(_file):
                    # logger.info(_mfld)
                    yield _file, _mfld

        return []

    def get_project_names(self) -> Iterable[Any]:
        _home = Path.home()
        x: Path
        y: Path
        for x, y in self.list_subspaces():

            yield x.stem, {"content": y, "config": x}

    def watchable_functions(self) -> Dict[str, Any]:
        return dict(list(self.get_project_names()))

    def remove_project(self, name: str):
        raise NotImplementedError

    def get_workspace_file(self):
        raise NotADirectoryError

    def workspace_dict(self):
        raise NotADirectoryError
