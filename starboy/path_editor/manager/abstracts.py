import abc
from pathlib import Path
from typing import Optional, Dict, Any, List, Iterable


class ProjectManagerAbs(abc.ABC):
    def __init__(self, lang_root_path: Path) -> None:
        self._lang_root_path: Path = lang_root_path

    @property
    def root_path(self) -> Path:
        return self._lang_root_path

    @property
    def root_fol_str(self) -> str:
        return str(self.root_file.absolute())

    @property
    def root_file(self):
        raise NotImplementedError

    @abc.abstractmethod
    def list_subspaces(self) -> Iterable[Path]:
        raise NotImplementedError

    def root_glob(self, glob_str: str) -> Iterable[Path]:
        return self.root_path.rglob(glob_str)

    def get_project_names(self):
        for x in self.list_subspaces():
            yield x.name

    @abc.abstractmethod
    def remove_project(self, name: str):
        raise NotImplementedError

    def get_workspace_file(self):
        raise NotADirectoryError

    def workspace_dict(self):
        raise NotADirectoryError