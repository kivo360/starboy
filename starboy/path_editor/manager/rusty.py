from loguru import logger
from pathlib import Path
from typing import Optional, Dict, Any, List, Iterable
from pydantic import BaseModel

import toml, shutil, os
import sh
import toolz
import typer


def process_output(line):
    logger.info(line)


class RustWorkspaceField(BaseModel):
    members: List[str] = []


class RustWorkspaceModel(BaseModel):
    workspace: RustWorkspaceField


def cmd(name: str):
    return sh.Command(name)


class RustProjectManager:
    """
        Rust Project Management
        ---
        Use this class to manage all of the projects for rust.
    """
    def __init__(self, rust_root_path: Path):
        """
        __init__

        Initialize the rust project manager with the rust root path.

        Parameters
        ----------
        rust_root_path : Path
            Sets the workspace path on __init__.
        
        
        """
        self._rust_root_path: Path = rust_root_path
        self._workspace_file: Optional[Path] = None

    @property
    def root_path(self) -> Path:
        return self._rust_root_path

    @property
    def root_fol_str(self) -> str:
        return str(self.root_path.absolute())

    @property
    def root_file(self):
        return self.get_workspace_file()

    @property
    def toml_dict(self) -> Dict[str, Any]:
        return self.load_workspace_toml()

    @property
    def workspace_model(self) -> RustWorkspaceModel:
        """
        Rust Workspace Model

        Runs a series of asserts to check to see if the workspace is a thing.

        Returns
        -------
        RustWorkspaceModel
            Get the workspace model.
        """
        return RustWorkspaceModel(**self.toml_dict)

    def get_workspace_file(self) -> Path:
        workspace_file_set = list(self._rust_root_path.glob("Cargo.toml"))

        if not workspace_file_set:
            raise AttributeError(
                "We weren't able to find the Cargo.toml workspace file.")
        self._workspace_file = Path(workspace_file_set[0])
        return self._workspace_file

    def load_workspace_toml(self) -> Dict[str, Any]:
        openned_file = self.root_file.open().read()
        return dict(toml.loads(openned_file))

    def update_workspace(self, wp_model: RustWorkspaceModel):
        tml = toml.dumps(wp_model.dict())
        with (self.root_path / "Cargo.toml").open('w', encoding="utf-8") as f:
            f.seek(0)
            f.write(tml)
            f.truncate()

    def cargo_install(self, name: str):
        cmd("cargo").install(name, _bg=True)

    def cargo_add(
        self,
        name: str,
        to_project: str,
        is_local=False,
    ):
        names = self.get_project_names()
        filtered_names = filter(lambda x: x == to_project, names)
        c = toolz.count(filtered_names)
        if c == 0:
            raise AttributeError("The project doesn't exist")
        project_path = (self.root_path / to_project)
        logger.warning(f"Adding project {name} to {to_project}")
        full_project_name = name
        if is_local:
            full_project_name = f"../{name}"

        cmd("cargo").add(full_project_name, _bg=True, _cwd=project_path)

    def cargo_remove(
        self,
        name: str,
        to_project: str,
        is_local=False,
    ):
        names = self.get_project_names()
        filtered_names = filter(lambda x: x == to_project, names)
        c = toolz.count(filtered_names)
        if c == 0:
            raise AttributeError("The project doesn't exist")
        project_path = (self.root_path / to_project)
        logger.warning(f"Removing project {name} from {to_project}")
        full_project_name = name
        if is_local:
            full_project_name = f"../{name}"

        cmd("cargo").rm(full_project_name, _bg=True, _cwd=project_path)

    def cargo_purge(
        self,
        name: str,
    ):
        typer.secho(fg=typer.colors.BRIGHT_CYAN)
        names = self.get_project_names()
        for name in names:
            project_path = (self.root_path / name)
            full_project_name = f"../{name}"
            cmd("cargo").rm(full_project_name, _bg=True, _cwd=project_path)

    def add_project(self, name: str, library=False):
        try:
            func = None
            if library:
                func = cmd("cargo").init(name,
                                         "--lib",
                                         _cwd=self.root_path,
                                         _bg=True)
                return
            func = cmd("cargo").init(name, _cwd=self.root_path, _bg=True)
            func.wait()
        except Exception as e:
            logger.error("Something done goofed up.")

    def remove_project(self, name: str):
        path = self.root_path / name
        shutil.rmtree(path)

    def list_subspaces(self) -> Iterable[Path]:
        """
        List Subspaces
        
        Get list of projects inside of the rust folder. Filtered if they don't have a cargo.toml file.
        """
        cargo_lower = "Cargo.toml".lower()
        for item in self.root_path.glob("*"):
            if item.is_dir():
                for i2 in item.glob("*"):
                    if i2.is_file() and i2.name.lower() == cargo_lower:
                        # Filtering files that are empty here.
                        if os.path.getsize(i2) > 0:
                            yield item

    def get_project_names(self) -> Iterable[str]:
        for x in self.list_subspaces():
            yield x.name

    def sync_subspace(self):
        """
            Sync Subspace

            Here we add members to the workspace. We do this to ensure we don't run into errors real-time.
        """
        local_wspace = self.workspace_model
        local_wspace.workspace.members = []

        for name in self.get_project_names():
            local_wspace.workspace.members.append(name)

        self.update_workspace(local_wspace)
