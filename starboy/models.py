# Standard Library
from typing import Any, Dict, List, Optional, Tuple, TYPE_CHECKING, Union

import toml

# Development Tools
from loguru import logger
from pathlib import Path

# Pydantic and Serialization
from pydantic import BaseModel
from pydantic import DirectoryPath
from pydantic import Extra
from pydantic import FilePath
from pydantic import validator

from starboy.path_editor import find_match_all
from starboy.path_editor import pants_toml

if TYPE_CHECKING:
    # Pydantic and Serialization
    from pydantic.typing import AbstractSetIntStr
    from pydantic.typing import DictStrAny
    from pydantic.typing import MappingIntStrAny


class PropertyBaseModel(BaseModel):
    """
    Workaround for serializing properties with pydantic until
    https://github.com/samuelcolvin/pydantic/issues/935
    is solved
    """
    def __init__(self, **data):
        super().__init__(**data)

        for getter, setter in self.get_properties():
            if getter in data and setter:
                getattr(type(self), setter).fset(self, data[getter])

    @classmethod
    def get_properties(cls):
        attributes = {prop: getattr(cls, prop) for prop in dir(cls)}
        properties = {
            name: attribute
            for name, attribute in attributes.items()
            if isinstance(attribute, property) and name not in ("__values__",
                                                                "fields")
        }

        setters = {
            prop.fget: name
            for name, prop in properties.items() if prop.fset
        }
        return [(name, setters.get(prop.fget))
                for name, prop in properties.items()
                if prop.fget and not prop.fset]

    def dict(self, *args, **kwargs) -> 'DictStrAny':
        self.__dict__.update({
            getter: getattr(self, getter)
            for getter, setter in self.get_properties()
        })

        return super().dict(*args, **kwargs)


class GLOBAL(BaseModel):
    pants_version: str
    print_stacktrace: bool
    pythonpath: List[str]
    backend_packages: List[str]


class Source(BaseModel):
    root_patterns: List[Path]

    def is_roots(self) -> bool:
        return len(self.root_patterns) > 0


class Isort(BaseModel):
    config: List[str]


class PantsModel(BaseModel):
    GLOBAL: GLOBAL
    source: Source
    isort: Isort

    class Config:
        arbitrary_types_allowed = True
        extra = Extra.allow

    def dict(self):
        d: dict = super().dict()
        d['source']['root_patterns'] = [
            str(r) for r in self.source.root_patterns
        ]
        return d


class PathTraversal(BaseModel):
    current_dir: DirectoryPath = Path().cwd()
    pants_config: Optional[FilePath] = None
    root_paths: Dict[str, DirectoryPath] = {}
    root_projects: Dict[str, List[str]] = {}
    pnt_root: Optional[DirectoryPath] = None

    class Config:
        extras = Extra.allow

    @validator("*", pre=True, always=True)
    def get_values_from_cache(cls, values):
        return values

    def find_project_root(self):
        if self.pnt_root is not None:
            return self.pnt_root

        curr = self.current_dir

        filtration_list = ['poetry.lock', 'pants.toml', 'pants']
        root = find_match_all(filtration_list, curr)
        self.pnt_root = root
        return root

    def get_pants_toml(self) -> FilePath:
        """
        Find Sub-Project Roots

        We take the pants project root folder then find all poetry subprojects and return a list of absolute paths.
        
        We use this list to get the relative paths to install the poetry projects into the super directory.
        """
        if self.pants_config is not None:
            return self.pants_config
        spr_prnt: Path = self.find_project_root()
        pnt_cfg = pants_toml(spr_prnt)
        self.pants_config: FilePath = pnt_cfg  # type: ignore
        if self.pants_config is None:
            raise AttributeError("Pants config is empty")
        return self.pants_config

    def get_root_patterns(self) -> List[Path]:
        pants_conf: PantsModel = self.load_pants_conf(
            modelled=True)  # type: ignore
        return pants_conf.source.root_patterns

    def load_pants_conf(self,
                        modelled: bool = False
                        ) -> Union[Dict[str, Any], PantsModel]:
        openned_toml = self.get_pants_toml().open('r', encoding="utf-8").read()
        loaded_toml = toml.loads(openned_toml)
        if not modelled:
            return loaded_toml  # type: ignore
        return PantsModel(**loaded_toml)

    def get_lang_root(self, rel_lang: Path) -> Tuple[Path, str]:
        lang = str(rel_lang).split("/")[-1]
        ldir = (self.find_project_root() / rel_lang)
        return ldir, lang

    def update_toml(self, pants_model: PantsModel):
        with self.get_pants_toml().open('w', encoding="utf-8") as f:
            f.seek(0)
            f.write(toml.dumps(pants_model.dict()))
            f.truncate()

    def get_root_by_name(self, name: str):
        lang_root_2: Optional[Path] = None
        # pants_conf: PantsModel = self.load_pants_conf(
        #     modelled=True)  # type: ignore
        loc_root = self.get_root_patterns()
        # assert loc_root == pants_conf.source.root_patterns
        for model in loc_root:
            lang_root, lang = self.get_lang_root(model)
            if lang == name:
                lang_root_2 = lang_root

        assert lang_root_2 is not None, f"There wasn't a root with the name: {name}"
        return lang_root_2


if __name__ == "__main__":
    hello = PathTraversal()
    logger.error(hello)

# {
#     message:
#     "success",
#     people: [{
#         name: "Sergey Ryzhikov",
#         craft: "ISS"
#     }, {
#         name: "Kate Rubins",
#         craft: "ISS"
#     }, {
#         name: "Sergey Kud-Sverchkov",
#         craft: "ISS"
#     }],
#     number:
#     3
# }
