from enum import Enum


class WatchType(str, Enum):
    MASTER = "master"
    FAAS = "faas"
    SERV = "service"
    COV = "covered"