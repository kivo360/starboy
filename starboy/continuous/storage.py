# Standard Library
from copy import deepcopy
from itertools import islice
# import json
import os
from typing import Any, Dict, List, Optional, Union

from watchdog.utils.dirsnapshot import DirectorySnapshot
from watchdog.utils.dirsnapshot import DirectorySnapshotDiff

# Development Tools
from devtools import debug
from loguru import logger
import pathlib
from pathlib import Path

# Pydantic and Serialization
import orjson
import pydantic
from pydantic import BaseModel
from pydantic import DirectoryPath
from pydantic import validate_arguments
from pydantic import validator

# Starboy
from starboy.continuous import WatchType
from starboy.snapshots.core import SnapshotEncoder
from starboy.snapshots.core import SnapshotWrapper
from starboy.types import DictStrAny
from starboy.utils import diff_dict
from starboy.utils import file_overwrite


def take(n, iterable):
    "Return first n items of the iterable as a list"
    if not iterable:
        return []
    return list(islice(iterable, n))


def ojdmp(v):
    # orjson.dumps returns bytes, to match standard json.dumps we need to decode
    return orjson.dumps(v).decode()


def orjson_dumps(v, *, default):
    # orjson.dumps returns bytes, to match standard json.dumps we need to decode
    return orjson.dumps(v, default=default).decode()


def default(obj):
    if isinstance(obj, Path):
        return obj.absolute().as_posix()


ojlod = orjson.loads

pydantic.json.ENCODERS_BY_TYPE[pathlib.PosixPath] = default
pydantic.json.ENCODERS_BY_TYPE[pathlib.WindowsPath] = default


class IntegrateStorage(BaseModel):
    bucket_root: Union[DirectoryPath, Path]

    # This is a snapshot of the entire monorepo.
    # We can use this to make comparisons with every key aspect.
    snapmon: SnapshotWrapper = SnapshotWrapper(is_skip=True)
    # FaaS functions that we're going to be watching.
    watched_faas: DictStrAny = {}
    # Services that we're also watching
    watched_services: DictStrAny = {}
    # Libraries and utils that go into support that we're watching.
    watched_covered: DictStrAny = {}

    @validator("snapmon", always=True)
    def reset_snapshot(cls, value):
        if value is None:
            raise AttributeError
        return value

    class Config:
        arbitrary_types_allowed = True
        json_loads = orjson.loads
        json_dumps = orjson_dumps

    @validator("bucket_root")
    def post_bucket_root(cls, bucket: Union[DirectoryPath, Path]):
        # logger.info(bucket).mkdir(parents=True, exist_ok=True)
        bucket.mkdir(parents=True, exist_ok=True)
        return bucket

    @property
    def snapshot_step(self) -> DictStrAny:
        sn = WatchType.MASTER
        self.file_reset(sn)
        return self.opn(sn)

    # @validate_arguments
    def file_reset(self,
                   wt: WatchType = WatchType.MASTER,
                   force: bool = False):
        flt = self.get_path(wt)
        if flt.suffixes:
            flt_parent = flt.parent
            if not flt_parent.exists() and not flt_parent.suffixes:
                flt_parent.mkdir(parents=True, exist_ok=True)
        else:
            if not flt.exists() and not flt.suffixes:
                flt.mkdir(parents=True, exist_ok=True)
        if not flt.is_file() or force:
            # print(self.opn(wt))
            # if wt != WatchType.MASTER:
            into_storage = self.dpdo(wt)

            file_overwrite(flt, into_storage)
        else:
            loaded_from_storage = self.opn(wt)
            self.set_watched(wt, loaded_from_storage)
            force = False
        # if force: file_overwrite(flt)

    def get_path(self, wpr: WatchType = WatchType.MASTER) -> Path:
        broot = self.bucket_root
        return {
            WatchType.MASTER: broot / "snapshot.json",
            WatchType.FAAS: broot / "faas-list.json",
            WatchType.SERV: broot / "services-list.json",
            WatchType.COV: broot / "coverage-network.json",
        }[wpr]

    def get_watched(self, wpr: WatchType = WatchType.FAAS) -> DictStrAny:
        return {
            WatchType.MASTER: self.snapmon,
            WatchType.FAAS: self.watched_faas,
            WatchType.SERV: self.watched_services,
            WatchType.COV: self.watched_covered,
        }[wpr]

    def set_watched(self,
                    wpr: WatchType = WatchType.FAAS,
                    value: Union[DictStrAny, DirectorySnapshot] = {}):
        curr = self.get_watched(wpr)
        if not value:
            return
        # debug(curr)
        return {
            WatchType.MASTER:
            lambda v: setattr(self, "snapmon", value),
            WatchType.FAAS:
            lambda v: setattr(self, "watched_faas", {
                **v,
                **curr
            }),
            WatchType.SERV:
            lambda v: setattr(self, "watched_services", {
                **v,
                **curr
            }),
            WatchType.COV:
            lambda v: setattr(self, "watched_covered", {
                **v,
                **curr
            }),
        }[wpr](value)

    def reset_watched(self,
                      wpr: WatchType = WatchType.FAAS,
                      value: DictStrAny = {}):
        # curr = self.get_watched(wpr)

        def _rs_by_name(_name: str, _val: Optional[dict] = {}):
            setattr(self, _name, _val)

        return {
            WatchType.MASTER:
            lambda v: _rs_by_name("snapmon", SnapshotWrapper(is_skip=True)),
            WatchType.FAAS:
            lambda v: _rs_by_name("watched_faas"),
            WatchType.SERV:
            lambda v: _rs_by_name("watched_services"),
            WatchType.COV:
            lambda v: _rs_by_name("watched_covered"),
        }[wpr](value)

    # @validate_arguments
    def dpdo(self, wt: WatchType = WatchType.MASTER) -> str:
        watch = self.get_watched(wt)
        if isinstance(watch, SnapshotWrapper):
            return watch.json()
        # updated_watch = take(10, watch)

        # debug(take(10, updated_watch))
        return orjson.dumps(watch,
                            option=orjson.OPT_NON_STR_KEYS
                            | orjson.OPT_SERIALIZE_NUMPY,
                            default=default).decode()

    # @validate_arguments
    def opn(
        self,
        wt: WatchType = WatchType.MASTER
    ) -> Union[DictStrAny, SnapshotWrapper]:
        """Open Value By Watch Type

        Args:
            wt (WatchType, optional): Lazily entered in for speed bitch. Defaults to WatchType.MASTER.

        Returns:
            DictStrAny: [description]
        """
        watch_config_path = self.get_path(wt)
        if wt == WatchType.MASTER:
            return SnapshotWrapper(is_skip=True).load_path(watch_config_path)
        return orjson.loads(self.get_path(wt).open().read())

    def reset(self):
        # Either creates a bunch of files or loads them.
        for wpr in WatchType:
            self.file_reset(wpr)
        return self

    def step(self,
             *args,
             faas_in: DictStrAny = {},
             snapshot_in: SnapshotWrapper = None,
             is_save: bool = True,
             **kwargs):
        # faas_data = )
        if faas_in:
            self.set_watched(value=faas_in)
        if snapshot_in is not None:
            self.set_watched(wpr=WatchType.MASTER, value=snapshot_in)
        if is_save:
            self.step_end()
        return self

    def step_end(self):
        """Saves all of the snapshot data at the end of step function.
        """
        for wpr in WatchType:
            # Force save the updates on close step. Will serve very well with
            # with logger.catch(Exception):
            self.file_reset(wpr, force=True)

    def flush(self):
        """Removes all of the state files and local variables
        """
        for wpr in WatchType:
            self.get_path(wpr).unlink(missing_ok=True)
            # os.remove(self.get_path(wpr))
            self.reset_watched(wpr)


# class StateDiffManager(BaseModel):
class StateDiffManager(BaseModel):
    new_store: IntegrateStorage
    old_store: IntegrateStorage

    class Config:
        arbitrary_types_allowed = True

    def get_data(self, wt: WatchType = WatchType.FAAS, is_new: bool = False):
        if is_new:
            return self.new_store.get_watched(wt)
        return self.old_store.get_watched(wt)

    @property
    def faas_diff(self) -> dict:
        return self.diff_top()

    @property
    def service_diff(self) -> dict:
        return self.diff_top(WatchType.SERV)

    @property
    def covered_diff(self) -> dict:
        return self.diff_top(WatchType.COV)

    @property
    def snap_diff(self) -> Any:
        older: SnapshotWrapper = self.old_store.get_watched(WatchType.MASTER)
        newer: SnapshotWrapper = self.new_store.get_watched(WatchType.MASTER)
        # logger.critical((older, newer))

        return (newer - older).merged_paths

    # @property
    def diff_top(self, watch_type: WatchType = WatchType.FAAS):
        older = self.old_store.get_watched(watch_type)
        newer = self.new_store.get_watched(watch_type)
        return diff_dict(older, newer)

    def reset(self):
        self.old_store.reset()
        self.new_store.reset()
        return self

    def step(self,
             *args,
             faas_in: DictStrAny = {},
             snapshot_in: SnapshotWrapper = None,
             **kwargs):
        # faas_in: DictStrAny = {},
        #  snapshot_in: DirectorySnapshot = None,
        #

        # placehold_snap = SnapshotWrapper(self.old_store.bucket_root)
        old_snapshot = self.get_data(WatchType.MASTER, is_new=True)
        # print(old_snapshot)
        old_faas = self.get_data(WatchType.FAAS, is_new=True)
        self.old_store.step(*args,
                            faas_in=old_faas,
                            snapshot_in=old_snapshot,
                            is_save=False,
                            **kwargs)
        self.new_store.step(*args,
                            faas_in=faas_in,
                            snapshot_in=snapshot_in,
                            **kwargs)
        # (state, reward, info)
        return self


@validate_arguments
def init_store(artifact_path: Path) -> StateDiffManager:
    tmp_store = IntegrateStorage(bucket_root=artifact_path)
    diman = StateDiffManager(old_store=tmp_store,
                             new_store=deepcopy(tmp_store))
    diman.reset()
    return diman


# def reverse_hash_dict(path_dict:dict):
