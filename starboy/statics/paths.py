import pathlib

import typer
from typer import colors
from pydantic import validate_arguments
HOME_FOLDER = pathlib.Path().home()


def appended_path(origin:pathlib.Path, *args):
    arg_list = (args)
    combined_paths = [origin] + arg_list
    return pathlib.Path(*combined_paths)


@validate_arguments
def home(*args) -> pathlib.Path:
    return appended_path(HOME_FOLDER, *args)
