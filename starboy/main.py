# Standard Library
from typing import Optional

import toml
import typer

# Development Tools
from loguru import logger

# Starboy
from starboy import devops
from starboy import models
from starboy import onboard
from starboy import projects
from starboy.path_editor.utils import appended_path

app = typer.Typer()


@logger.catch(reraise=True)
@app.command('create')
def create_project():
    """
        Gets the root project level then traverses down src/python to be able to create a new project with poetry.

        Get the relative directory of this new project from the root poetry project this project will add it via poetry add.
    """
    traverser = models.PathTraversal()
    path = traverser.find_project_root()
    p = appended_path(path, "pants.toml")
    if p.exists():
        pass

    # logger.info(p)


@app.command('trial')
def get_location():
    """Gets the current logitiude and latitude of the spacecraft"""
    print()


app.add_typer(onboard.app, name="onboard", help="Setup your dev environment")
app.add_typer(devops.app,
              name="devops",
              help="Use this tool to automatically setup mandetory variables.")
app.add_typer(projects.app, name="projects", help="Use to manage the projects")


def main():
    app()


if __name__ == "__main__":
    app()
