# Standard Library
from copy import deepcopy
from functools import cached_property

import sh
from sh import Command


class KubeConfigAdapter:
    @cached_property
    def kubectl(self):
        return sh.Command("kubectl")

    @cached_property
    def config(self):
        return deepcopy(self.kubectl).bake("config")

    @cached_property
    def config_view(self):
        return deepcopy(self.config).bake("view")

    @cached_property
    def get(self):
        return deepcopy(self.kubectl).bake("get")

    @cached_property
    def create(self):
        return deepcopy(self.kubectl).bake("create")

    @cached_property
    def kube_system(self):
        return deepcopy(self.kubectl).bake("-n", "kube-system")

    def kube_namespace(self, namespace: str):
        return deepcopy(self.kubectl).bake("-n", namespace)
