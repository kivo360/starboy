# Standard Library
from copy import deepcopy
import os
from typing import Any, Dict, List, Optional, Tuple, TYPE_CHECKING, Union

from dictdiffer import diff
from watchdog.utils.dirsnapshot import DirectorySnapshot
from watchdog.utils.dirsnapshot import DirectorySnapshotDiff

# Development Tools
from devtools import debug
from loguru import logger
from pathlib import Path

# Pydantic and Serialization
import orjson
from pydantic import BaseModel
from pydantic import errors
from pydantic import utils
from pydantic.validators import tuple_validator

if TYPE_CHECKING:
    # Standard Library
    from inspect import Signature

    import typing_extensions

    # Pydantic and Serialization
    from pydantic.class_validators import ValidatorListDict
    from pydantic.types import ModelOrDc
    from pydantic.typing import AbstractSetIntStr  # noqa: F401
    from pydantic.typing import CallableGenerator
    from pydantic.typing import MappingIntStrAny
DictStrAny = Dict[str, Any]
ListAny = List[Any]
UnionComposite = Union[ListAny, DictStrAny]


def create_status_dict(stat_result: os.stat_result):
    return {
        k: getattr(stat_result, k)
        for k in dir(stat_result) if k.startswith('st_')
    }


SAME_BRAIN = '/home/aguman/Software/LearningProjects/Rust/MonoRepo/src/faas'

# os.fstat
cov_lstat = create_status_dict


class OSStatResultValues(BaseModel):
    st_mode: int
    st_ino: int
    st_dev: int
    st_nlink: int
    st_uid: int
    st_gid: int
    st_size: int
    st_atime: int  # time of most recent access,
    st_mtime: int  # time of most recent content modification,
    st_ctime: int  # platform dependent (time of most recent metadata change on Unix, or the time of creation on Windows)
    st_atime_ns: int
    st_mtime_ns: int
    st_ctime_ns: int

    st_blksize: Optional[int] = None  # filesystem blocksize
    st_blocks: Optional[int] = None  # number of blocks allocated for file
    st_rdev: Optional[int] = None  # type of device if an inode device
    st_flags: Optional[int] = None  # user defined flags for file
    st_gen: Optional[int] = None  # file generation number
    st_birthtime: Optional[int] = None  # time of file creation
    st_rsize: Optional[int] = None
    st_creator: Optional[int] = None
    st_type: Optional[int] = None

    tmp_os_storage: Optional[Any] = None

    def __init__(self, os_result: os.stat_result = None, **data):
        if os_result is not None:
            os_dict = cov_lstat(os_result)
            # debug(os_dict)
            # logger.info("converting into an internal model.")
            # debug(os_result)
            data.update(os_dict)
        super().__init__(**data)
        self.tmp_os_storage = os_result

    def get_tuple(self):

        tup_vals = (
            self.st_mode,
            self.st_ino,
            self.st_dev,
            self.st_nlink,
            self.st_uid,
            self.st_gid,
            self.st_size,
            self.st_atime_ns,
            self.st_mtime_ns,
            self.st_ctime_ns,
            # self.st_mtime_ns,
            # self.st_ctime_ns,
            # self.st_atime_ns,
            # self.st_blksize,
            # self.st_blocks,
            # self.st_rdev,
            # self.st_flags,
            # self.st_gen,
            # self.st_birthtime,
            # self.st_rsize,
            # self.st_creator,
            # self.st_type,
            # self.tmp_os_storage,
        )
        # tup_vals = tuple(filter(lambda x: x is not None, tup_vals))
        return tup_vals

    def get_stat_result(self):
        os_tuple = self.get_tuple()
        stat_res = os.stat_result(os_tuple)
        return stat_res


class OSStatResultValuesBack(BaseModel):
    st_mode: int
    st_ino: int
    st_dev: int
    st_nlink: int
    st_uid: int
    st_gid: int
    st_size: int
    st_atime: float  # time of most recent access,
    st_mtime: float  # time of most recent content modification,
    st_ctime: float  # platform dependent (time of most recent metadata change on Unix, or the time of creation on Windows)
    st_atime_ns: int
    st_mtime_ns: int
    st_ctime_ns: int

    # On some Unix systems (such as Linux), the following attributes may also
    # be available:
    st_blocks: Optional[int]  # number of blocks allocated for file
    st_blksize: Optional[int]  # filesystem blocksize
    st_rdev: Optional[int]  # type of device if an inode device
    st_flags: Optional[int]  # user defined flags for file

    # On other Unix systems (such as FreeBSD), the following attributes may be
    # available (but may be only filled out if root tries to use them):
    st_gen: Optional[int]  # file generation number
    st_birthtime: Optional[int]  # time of file creation

    # On Mac OS systems, the following attributes may also be available:
    st_rsize: Optional[int]
    st_creator: Optional[int]
    st_type: Optional[int]

    def __init__(self, os_result: os.stat_result = None, **data):
        if os_result is not None:
            os_dict = create_status_dict(os_result)
            debug(os_dict)
            # data.update(os_dict)
        super().__init__(**data)

    def get_tuple(self):
        return (self.st_mode, self.st_ino, self.st_dev, self.st_nlink,
                self.st_uid, self.st_gid, self.st_size, self.st_atime,
                self.st_mtime, self.st_ctime, self.st_mtime_ns,
                self.st_ctime_ns, self.st_atime_ns, self.st_blocks,
                self.st_blksize, self.st_rdev)

    def get_stat_result(self):
        os_tuple = self.get_tuple()
        # debug(os_tuple)
        # debug(os_tuple)
        return os.stat_result(self.get_tuple())


class OSStatResult(tuple):
    __validation__ = None
    min_length = 10
    max_length = 22

    @classmethod
    def __modify_schema__(cls, field_schema: Dict[str, Any]) -> None:
        utils.update_not_none(
            field_schema,
            minLength=cls.min_length,
            maxLength=cls.max_length,
        )

    @classmethod
    def __get_validators__(cls) -> 'CallableGenerator':
        yield cls.validate

    @classmethod
    def validate(cls, value: Any) -> 'OSStatResult':
        try:

            if isinstance(value, cls):
                return value

            if isinstance(value, os.stat_result):
                cls.__validation__ = OSStatResultValues(value)
                return cls(cls.__validation__.get_stat_result())
            if isinstance(value, dict):
                dict_len = len(value)
                if dict_len < cls.min_length:
                    raise errors.TupleLengthError(
                        actual_length=dict_len, expected_length=cls.min_length)
                elif dict_len > cls.max_length:
                    raise errors.TupleLengthError(
                        actual_length=dict_len, expected_length=cls.max_length)
                cls.__validation__ = OSStatResultValues(value)
                return cls(cls.__validation__.get_stat_result())
            if utils.sequence_like(value):
                value = tuple(value)
            if isinstance(value, tuple):
                tup_len = len(value)
                if tup_len < cls.min_length:
                    raise errors.TupleLengthError(
                        actual_length=tup_len, expected_length=cls.min_length)
                elif tup_len > cls.max_length:
                    raise errors.TupleLengthError(
                        actual_length=tup_len, expected_length=cls.max_length)

            value = os.stat_result(value)
            return cls(value)
        except Exception as e:
            logger.exception(e)
            raise e

    def __repr__(self) -> str:
        return f"OSStats('{self}')"

    def __str__(self) -> str:
        return str(self.__validation__)

    def get_os_stat(self) -> os.stat_result:
        return self.__validation__.get_stat_result()


class SnapField(BaseModel):
    file_path: Path
    os_stats: OSStatResult
    inode_id: Tuple[int, int]

    class Config:
        json_encoders = {
            'file_path': lambda v: str(v),
            'os_stats': lambda z: z.dict(exclude_none=True),
        }
