# Use models to convert data into a standard type
from typing import Dict, Any, Optional, Union
from pathlib import Path
from pydantic import BaseModel

DictStrPath = Dict[str, Path]
DictStrAny = Dict[str, Any]
DoubleDictPath = Dict[str, DictStrPath]
TripleDictPath = Dict[str, DoubleDictPath]


class PathInput(BaseModel):
    _input: Union[TripleDictPath, DoubleDictPath, DictStrPath, DictStrAny]
