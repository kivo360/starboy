from io import TextIOWrapper
from pathlib import Path
from typing import Dict, Any, Type, List, Tuple, Optional
from dictdiffer import diff, patch, swap, revert, dot_lookup
DictDiff = List[Tuple[str, str, Tuple[Any]]]


def add_key(origin: dict, key: str) -> dict:
    _placeholder = {}
    _placeholder[key] = origin
    return _placeholder


def diff_dict_gen(diff_result: DictDiff) -> Dict[str, Any]:
    def blank_node(key: str):
        output: Dict[str, Any] = {}
        dsplit = key.split(".")
        if len(dsplit) < 2:
            # NOTE: If there's only the key we just returned the updated object.
            return add_key(output, key)
        for key in reversed(key.split(".")):
            output = add_key(output, key)
        return output

    def empty_aggregate(_diff_res: DictDiff):
        combined = {}
        for action, node, changes in _diff_res:

            combined.update(blank_node(node))
        return combined

    # NOTE: Going to use the difference of repositories and this dict to determine what needs to get disabled
    agg_target = empty_aggregate(diff_result)
    return patch(diff_result, agg_target)


def diff_dict(first: dict, second: dict) -> Dict[str, Any]:
    """Get a dictionary that has difference between file names.

    Args:
        first (dict): The original dictionary.
        second (dict): The newly loaded dictonary

    Returns:
        Dict[str, Any]: All of the remaining values
    """
    return diff_dict_gen(diff(first=first, second=second))