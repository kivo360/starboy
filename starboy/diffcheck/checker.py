# Standard Library
from copy import copy
from copy import deepcopy
from typing import Any, Tuple

# Development Tools
import addict as adt
from pathlib import Path

# Pydantic and Serialization
from pydantic import validate_arguments
from pydantic.main import BaseModel

from starboy.continuous import IntegrateStorage
from starboy.continuous import WatchType
from starboy.diffcheck.codegen import diff_dict
from starboy.types import DictStrAny


class StateDiffManager(BaseModel):
    new_store: IntegrateStorage
    old_store: IntegrateStorage

    class Config:
        arbitrary_types_allowed = True

    @property
    def full_faas(self):
        self.new_store.get_watched()

    @property
    def faas_diff(self) -> dict:
        return self.diff_top()

    @property
    def service_diff(self) -> dict:
        return self.diff_top(WatchType.SERV)

    @property
    def covered_diff(self) -> dict:
        return self.diff_top(WatchType.COV)

    def diff_top(self, watch_type: WatchType = WatchType.FAAS):
        older = self.old_store.get_watched(watch_type)
        newer = self.new_store.get_watched(watch_type)
        return diff_dict(older, newer)

    def reset(self):
        self.old_store.reset()
        self.new_store.reset()

    def step(self, *args, **kwargs):
        self.new_store.step(**kwargs)


@validate_arguments
def init_store(artifact_path: Path) -> StateDiffManager:
    tmp_store = IntegrateStorage(bucket_root=artifact_path)
    return StateDiffManager(old_store=tmp_store, new_store=deepcopy(tmp_store))
