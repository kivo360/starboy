# Standard Library
import shutil

# Development Tools
import devtools as deev
from pathlib import Path

# Starboy
from starboy.snapshots.core import SnapshotWrapper
from starboy.steps import get_faas_resources
from starboy.steps import init_state_manager

# from starboy.types import

BUCKET = "/tmp/config_bucket"


def test_resources():
    if Path(BUCKET).exists():
        shutil.rmtree(BUCKET)
    plh = SnapshotWrapper(is_skip=True)
    plz = SnapshotWrapper(is_skip=True)
    # state = init_state_manager(BUCKET)

    top_lvl, snap = get_faas_resources(BUCKET)
    # deev.debug(state.old_faas_snap)

    # assert state.old_faas_snap == plh
