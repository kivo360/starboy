# Standard Library
import copy
import tempfile

import pytest

# Development Tools
from devtools import debug
from pathlib import Path

# Starboy
from starboy.continuous.enums import WatchType
from starboy.continuous.storage import IntegrateStorage

S3_BUCKET = Path("/tmp/test_bucket")


@pytest.fixture
def integrated_bucket():
    return IntegrateStorage(bucket_root=S3_BUCKET)


@pytest.fixture(scope="module")
def old_faas():
    return {
        "dingo": {
            "content": Path("/tmp/placeholder/dingo"),
            "config": Path("/tmp/placeholder/dingo.yml")
        },
        "star_man": {
            "content": Path("/tmp/placeholder/star_man"),
            "config": Path("/tmp/placeholder/star_man.yml")
        },
        "space_ghost": {
            "content": Path("/tmp/placeholder/space_ghost"),
            "config": Path("/tmp/placeholder/space_ghost.yml")
        },
    }


@pytest.fixture(scope="module")
def new_faas():
    return {
        "dingo": {
            "content": Path("/tmp/placeholder/dingo"),
            "config": Path("/tmp/placeholder/dingo.yml")
        },
        "star_man": {
            "content": Path("/tmp/placeholder/star_man"),
            "config": Path("/tmp/placeholder/star_man.yml")
        },
        "space_ghost": {
            "content": Path("/tmp/placeholder/space_ghost"),
            "config": Path("/tmp/placeholder/space_ghost.yml")
        },
        "liono": {
            "content": Path("/tmp/placeholder/space_ghost"),
            "config": Path("/tmp/placeholder/space_ghost.yml")
        },
        "heman": {
            "content": Path("/tmp/placeholder/space_ghost"),
            "config": Path("/tmp/placeholder/space_ghost.yml")
        },
    }


@pytest.fixture(scope="module")
def int_old(integrated_bucket, old_faas):
    storage = copy.deepcopy(integrated_bucket)

    # storage.flush()
    storage.reset()
    storage.set_watched(WatchType.FAAS, old_faas)
    return storage


@pytest.fixture(scope="module")
def int_new(integrated_bucket, new_faas):
    """Get a loaded bucket with updated faas functions.

    Args:
        integrated_bucket (IntegrateStorage): The variables where we store path and snapshot information.
        new_faas (Dict[str, Dict[str, Path]]): Representing a changed path compared to the old

    Returns:
        IntegrateStorage: Storage baby
    """
    storage = copy.deepcopy(integrated_bucket)
    storage.flush()
    storage.reset()
    storage.set_watched(WatchType.FAAS, new_faas)
    return storage
