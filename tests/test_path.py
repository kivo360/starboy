import time
from pathlib import Path
from typing import Optional

from devtools import debug
from loguru import logger
from starboy.models import PantsModel, PathTraversal
from starboy.path_editor import RustProjectManager
from starboy.path_editor.manager.faas import FaasManager
from devtools import debug


def test_pants_root():
    traveller = PathTraversal()
    pants_path = traveller.find_project_root()
    assert pants_path.exists(
    ), "For some reason the project folder doesn't exist."

    # Get the pants.toml
    pants_toml = traveller.get_pants_toml()
    assert pants_toml.exists(), "Pants file does NOT exist."

    # Load the reachable sets
    conf_dict = traveller.load_pants_conf()
    assert conf_dict is not None, "We were't able to load the configuration."
    assert isinstance(conf_dict,
                      dict), "The configuration dict wasn't able to be loaded."


def test_pants_config_state():
    traveller = PathTraversal()
    conf_dict = traveller.load_pants_conf()
    pants_model = PantsModel(**conf_dict)

    assert pants_model.source.root_patterns, "We weren't able to find root patterns."

    # First source root shouldn't exist if you aren't currently inside of the root folder.
    first_root = pants_model.source.root_patterns[0]
    if Path().cwd() is not traveller.find_project_root():
        assert not first_root.exists(
        ), "The relative path shouldn't exist here."

    # The first language root should exist. Throw an error if it doesn't exist
    lang_root, lang = traveller.get_lang_root(first_root)
    lang_root.exists(), "The language root should exist."
    lang_root.is_dir(), "The given language root does exist."
    assert isinstance(lang, str), "This language is a string."

    # Add a build path to the toml file.
    # Add to pythonpath
    curr_len = len(pants_model.GLOBAL.pythonpath)

    pants_model.GLOBAL.pythonpath.append("sample_path")

    new_len = (pants_model.GLOBAL.pythonpath)

    assert new_len != curr_len, "The number of items inside of the pants.toml file shouldn't be the exact same."

    traveller.update_toml(pants_model)

    conf_dict = traveller.load_pants_conf()
    pants_model = PantsModel(**conf_dict)
    del pants_model.GLOBAL.pythonpath[-1]
    assert curr_len == len(pants_model.GLOBAL.pythonpath)
    traveller.update_toml(pants_model)


def test_rust_project():
    traveller = PathTraversal()
    rust_root: Optional[Path] = traveller.get_root_by_name("rust")

    # Check for the first workspace project
    rust_pm: RustProjectManager = RustProjectManager(rust_root)

    ws_model = rust_pm.workspace_model
    assert ws_model, "For some reason the model isn't returning and it's still not raising an exception."

    # create a project name toby
    project_name: str = "toby"
    new_proj_path = rust_root / project_name

    assert not new_proj_path.exists(
    ), "The project does exist where it shouldn't."

    rust_pm.add_project(name=project_name)
    rust_pm.sync_subspace()
    time.sleep(0.3)
    assert new_proj_path.exists(), "The project does exist where it shouldn't."
    assert new_proj_path.is_dir(), "The project does exist where it shouldn't."

    rust_pm.remove_project(name=project_name)
    assert not new_proj_path.exists(
    ), "The project does exist where it shouldn't"

    # Sync workspaces
    # Ensures we have all of the rust projects inside of the system.
    rust_pm.sync_subspace()

    # Kunta-kinte
    rust_pm.cargo_install("cargo-edit")

    # Solo-Northup
    rust_pm.cargo_add("dashmap", "service_bodhi")


def test_find_yaml():
    traveller = PathTraversal()
    faas_root: Optional[Path] = traveller.get_root_by_name("faas")
    faas_manager = FaasManager(faas_root)
    debug(faas_manager.watchable_functions())

    assert True
