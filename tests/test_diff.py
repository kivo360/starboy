# Development Tools
from loguru import logger
from pathlib import Path

# Starboy
from starboy.continuous import IntegrateStorage
from starboy.continuous import StateDiffManager


def test_set_bucket(integrated_bucket: IntegrateStorage):
    assert integrated_bucket.bucket_root.exists()


def test_toplevel_diff(int_old: IntegrateStorage, int_new: IntegrateStorage):
    diffy = StateDiffManager(new_store=int_new, old_store=int_old)
    diffy.diff_top()
    assert True
