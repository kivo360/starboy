# Standard Library
import os
import tempfile

# Development Tools
from devtools import debug
from loguru import logger
from pathlib import Path

# Starboy
from starboy.snapshots.core import SnapshotWrapper
from starboy.snapshots.core import TotalChange
from starboy.types import create_status_dict
from starboy.types import OSStatResultValues

cov_lstat = create_status_dict


def test_lstat_order():
    current_dir = Path().cwd()
    lstat = current_dir.lstat()
    os_stat_vals = OSStatResultValues(lstat)
    updated_lstat = os_stat_vals.get_stat_result()
    first = cov_lstat(lstat)
    second = cov_lstat(updated_lstat)

    assert second == second


# def test_added_file_info():
#     test_path = "/home/aguman/Software/LearningProjects/Rust/MonoRepo/src"
#     temp_file = Path(test_path) / "temp_example.txt"
#     # temp_file.unlink(missing_ok=True)\
#     changes: TotalChange = None
#     with tempfile.TemporaryDirectory() as temp_dir:
#         temp_file = Path(temp_dir) / "temp_example.txt"
#         logger.info(temp_dir)
#         for i in range(3):
#             prev = SnapshotWrapper(temp_dir)
#             with temp_file.open("a+") as f:
#                 for i in range(1000):
#                     f.write("shit dick \n")
#             after = SnapshotWrapper(temp_dir)
#             changes = TotalChange((after - prev))
#             logger.info(changes)
#         assert len(changes.merged_paths) > 0
#     # # temp_file.unlink(missing_ok=True)
#     # logger.info(changes)
