# Standard Library
from typing import Dict

from pytest_mock import MockerFixture

# Development Tools
from loguru import logger
from pathlib import Path

# Starboy
from starboy.continuous import IntegrateStorage
from starboy.continuous import WatchType
from starboy.snapshots.core import SnapshotWrapper


def test_set_bucket(integrated_bucket: IntegrateStorage):
    assert integrated_bucket.bucket_root.exists()


def test_reset_storage(integrated_bucket: IntegrateStorage,
                       old_faas: Dict[str, Dict[str, Path]]):
    faas_type = WatchType.FAAS
    mas_path = integrated_bucket.get_path(WatchType.MASTER)
    # logger.error(mas_path.open().read())
    integrated_bucket.flush()
    integrated_bucket.reset()
    assert mas_path.exists()
    integrated_bucket.set_watched(faas_type, old_faas)
    assert bool(integrated_bucket.get_watched(faas_type))
    integrated_bucket.step_end()
    integrated_bucket.reset()
    integrated_bucket.flush()
    assert not bool(integrated_bucket.get_watched(faas_type))


def test_snapshot_integration(integrated_bucket: IntegrateStorage, old_faas,
                              mocker: MockerFixture):
    snapshot_home = "/home/aguman/Software/LearningProjects/Rust/MonoRepo/src/faas"
    snapshot = SnapshotWrapper(snapshot_home)
    snap_type = WatchType.MASTER
    buck_path = integrated_bucket.get_path(snap_type)
    integrated_bucket.flush()
    assert not buck_path.exists()
    integrated_bucket.reset()

    assert buck_path.exists()
    integrated_bucket.step(snapshot_in=snapshot, faas_in=old_faas)
    integrated_bucket.reset()

    # logger.info(buck_path.open().read())
    # integrated_bucket.set_watched(snap_type, snapshot)
    # integrated_bucket.dpdo(snap_type)
    # # logger.info(outcome)
    # outcome = integrated_bucket.opn(snap_type)
    # logger.info(outcome)
    # integrated_bucket.file_reset(snap_type)

    # assert depedo_call.assert_called()
    # assert bool(integrated_bucket.get_watched(snap_type))
    # integrated_bucket.step_end()
    # integrated_bucket.reset()
    # integrated_bucket.flush()
    # assert not bool(integrated_bucket.get_watched(snap_type))


# def test_snapshot_integration(integrated_bucket: IntegrateStorage,
#                               old_faas: dict, mocker: MockerFixture):
#     snapshot_home = "/home/aguman/Software/LearningProjects/Rust/MonoRepo/src/faas"
#     snapshot = SnapshotWrapper(snapshot_home)
#     snap_type = WatchType.MASTER
#     buck_path = integrated_bucket.get_path(snap_type)
#     integrated_bucket.flush()
#     assert not buck_path.exists()
#     integrated_bucket.reset()

#     assert buck_path.exists()
#     integrated_bucket.step(snapshot_in=snapshot, faas_in=old_faas)
#     integrated_bucket.reset()

#     # logger.info(buck_path.open().read())
# integrated_bucket.set_watched(snap_type, snapshot)
# integrated_bucket.dpdo(snap_type)
# # logger.info(outcome)
# outcome = integrated_bucket.opn(snap_type)
# logger.info(outcome)
# integrated_bucket.file_reset(snap_type)

# assert depedo_call.assert_called()
# assert bool(integrated_bucket.get_watched(snap_type))
# integrated_bucket.step_end()
# integrated_bucket.reset()
# integrated_bucket.flush()
# assert not bool(integrated_bucket.get_watched(snap_type))
