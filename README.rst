
Personalized CI/CD Management Tool
==================================

Huge warning and note to self
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The code inside of this code base is highly fragmented and needs to, at some point, get re-organized. The re-organization will largely be centered around removing design patterns, and compressing certain files.

For example, the logic for traversal through the entire monorepo is split between numerous files. When looking at something like pydantic, we see that this is not necessarily warranted. What the author does inside of that library is concentrate the application into just a few files. Each file has a theme and doesn't deviate too much. The number of lines per file rarely exceeds 500 lines. Yet, every so often the number of lines extends to 900+. Deciding to make the trade-off of more lines actually makes the code more findable. That's because the surface area of where the

Even Bigger Note
^^^^^^^^^^^^^^^^

I could have used pantsbuild to create more stuff than I realized if I just understood the way they did things. Will finish this project here to learn the final bits of this work and have the assets for a later time. However, with ``pantsbuild`` still inside of the repo, it might be useful to extend some plugins to that library while splitting the current starboy library into smaller repos. The ``pantsbuild`` library can be triggered using starboy.

To Install:
^^^^^^^^^^^

.. code-block:: bash

   pip install starboy

Then run this command to get started:

.. code-block:: bash

   starboy --help
